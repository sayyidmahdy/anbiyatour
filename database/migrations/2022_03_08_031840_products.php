<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Products extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id')->length(11);
            $table->string('title');
            $table->longtext('description');
            $table->float('price', 10, 2);
            $table->integer('flight')->length(1);
            $table->integer('type')->length(1);
            $table->integer('qty')->length(10);
            $table->string('image');
            $table->string('image_path');
            $table->string('slug');
            $table->integer('status')->length(1);
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->default(DB::raw('NULL ON UPDATE CURRENT_TIMESTAMP'))->nullable();
        });      }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
