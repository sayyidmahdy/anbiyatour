function input__validator(formid)
{
	var err = 0;

    $('#'+formid).find(':input').each(function(){
	    
	    var elm = $(this);
	    var label = (elm.closest('.form-group').find('label').text()).replace('*', '');

	    elm.closest('.form-control').removeClass('is-invalid');
        elm.closest('.form-group').find('.invalid-feedback').html('');
    	
    	if(elm.val().trim() === '')
    	{
    		if(elm.attr('required'))
    		{
		        err++;
	    		elm.closest('.form-control').addClass('is-invalid');
	    		elm.closest('.form-group').find('.invalid-feedback').html(label+' harus diisi');
	    	}
	    }
	    else
	    {

	    	if(elm.attr('type') == 'email')
			{
				if(!validateEmail(elm.val()))
				{
					err++;
		            elm.closest('.form-control').addClass('is-invalid');
		    		elm.closest('.form-group').find('.invalid-feedback').html('Email tidak valid');
				}
			} 
            else {
                elm.closest('.form-control').removeClass('is-invalid');
                elm.closest('.form-control').addClass('is-valid');

                elm.closest('.form-group').find('.invalid-feedback').addClass('valid-feedback');
                elm.closest('.form-group').find('.valid-feedback').removeClass('invalid-feedback');
                elm.closest('.form-group').find('.valid-feedback').html('Looks good!');
            }

	    }
	});

    if(err > 0)
    	return response = {fail:true};
    else
    	return response = {fail:false};

}

function input_group_onkeyup__validator(elm)
{
    elm.closest('.form-group').removeClass('has-danger');
    elm.closest('.form-group').find('.invalid-feedback').html('');

    if(elm.attr('name') == 'nama')
	{
		if (checknama(elm) != 'true')
		{
			elm.closest('.form-control').addClass('is-invalid');
    		elm.closest('.form-group').find('.invalid-feedback').html(checknama(elm));
		}
	}

	if(elm.attr('type') == 'email')
	{
		if(!validateEmail(elm.val()))
		{
            elm.closest('.form-control').addClass('is-invalid');
    		elm.closest('.form-group').find('.invalid-feedback').html('Email tidak valid');
		}
	}

	if(elm.attr('type') == 'password')
	{	
		
		if (checkpassword(elm) != 'true')
		{
			elm.closest('.form-control').addClass('is-invalid');
    		elm.closest('.form-group').find('.invalid-feedback').html(checkpassword(elm));
		}
	}

	if(elm.attr('name') == 'password_confirmation')
	{
		var password = elm.closest('form').find('input[name=password]').val();
		var c_password = elm.val();

		if(password != c_password)
		{
			elm.closest('.form-control').addClass('is-invalid');
    		elm.closest('.form-group').find('.invalid-feedback').html('Password tidak sama');
		}
	}

	if(elm.attr('name') == 'npwp')
	{
		var npwp = ReplaceAll(ReplaceAll(elm.val(), '-', ''), '.', '');
		if(npwp.length < 15)
		{
			elm.closest('.form-control').addClass('is-invalid');
    		elm.closest('.form-group').find('.invalid-feedback').html('NPWP tidak valid');
		}
	}
}

function checknama(elm)
{
	var nama = elm.val();
	if (nama.search(/[\!\@\#\$\%\^\&\*\(\)\-\=\_\+\.\,\<\>\;\:\/\?\`\~]/) >= 0)
    {
        return 'Nama tidak valid';
    }
    else
    {
        return 'true';
    }
}

function validateEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

function checkpassword(elm)
{
	var str = elm.val();

	if (str.length < 8)
    {
        return 'Password minimal 8 karakter';
    }
    else if (str.length > 32)
    {
        return 'Password maksimal 32 karakter';
    }
    else if (str.search(/\d/) == -1)
    {
        return 'Password harus mengandung angka';
    }
    else if (str.search(/[a-z]/) == -1)
    {
        return 'Password harus mengandung huruf kecil';
    }
    else if (str.search(/[A-Z]/) == -1)
    {
        return 'Password harus mengandung huruf kapital';
    }
    else if (str.search(/[\!\@\#\$\%\^\&\*\(\)\-\=\_\+\.\,\<\>\;\:\/\?]/) == -1)
    {
        return 'Password harus mengandung spesial karakter';
    }
    else
    {
    	return 'true';
    }
}

function password_cek()
{
	var str 		= $('#password').val();
	var password2	= $('#password_confirmation').val();

	if (str.length < 8) {
        return [
	        $('#password_input').removeClass('row'),
        	$('#password_input').addClass('has-danger row'),
        	$('#info_cek').text('Password minimal 8 karakter')];
      
    } else if (str.length > 32) {
        return [
        	$('#password_input').removeClass('row'),
        	$('#password_input').addClass('has-danger row'),
        	$('#info_cek').text('Password maksimal 32 karakter')];
    } else if (str.search(/\d/) == -1) {
        return [
        	$('#password_input').removeClass('row'),
        	$('#password_input').addClass('has-danger row'),
        	$('#info_cek').text('Password harus mengandung angka')];
    } else if (str.search(/[a-z]/) == -1) {
        return [
        	$('#password_input').removeClass('row'),
        	$('#password_input').addClass('has-danger row'),
        	$('#info_cek').text('Password harus mengandung huruf kecil')];
    } else if (str.search(/[A-Z]/) == -1) {
        return [
        	$('#password_input').removeClass('row'),
        	$('#password_input').addClass('has-danger row'),
        	$('#info_cek').text('Password harus mengandung huruf kapital')];
  	}else if (str.search(/[\!\@\#\$\%\^\&\*\(\)\-\=\_\+\.\,\<\>\;\:\/\?]/) == -1) {
        return [
        	$('#password_input').removeClass('row'),
        	$('#password_input').addClass('has-danger row'),
        	$('#info_cek').text('Password harus mengandung spesial karakter')];
    }
    return [
        $('#password_input').removeClass('has-danger'),
        $('#info_cek').empty()];
}

function password_confirmation()
{
	var password1 	= $('#password').val();
	var password2	= $('#rePassword').val();
	
	if (password1 != password2) {
    	$('#password_confirm').addClass('has-danger');
    	$('#confirm_cek').text('Pengulangan password tidak benar');
    	$('#btn_submit').attr('disabled', 'disabled');
    } else if (password1 == password2) {
    	$('#password_confirm').removeClass('has-danger');
    	$('#confirm_cek').empty();
    	$('#btn_submit').prop("disabled", false);
    }
}
