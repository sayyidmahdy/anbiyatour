function show_form(id, url, detail_id, create_url){
	if (detail_id) {
		var body_url = base_url + url + detail_id;
		var url 	 = create_url;
		var breadcrumb = 'Update';
	} else {
		var body_url = base_url + url;
		var breadcrumb = 'Create';
	}
	NProgress.start();
	$('#btn_' + id).prop('disabled', true);
	$('#div_add_' + id).load(body_url,
	    function () {
	        NProgress.done();
	    	$('#btn_' + id).prop('disabled', false);
	        $('#div_add_' + id).slideDown();
			$('#btn_' + id).attr('onclick', 'close_form(\''+ id +'\', \''+ url +'\')');
			$('#text_' + id).text('Close Form');
			$('#breadcrumb_' + id).text(breadcrumb);
			$('#icon_' + id).removeClass('mdi mdi-account-plus-outline');
			$('#icon_' + id).addClass('mdi mdi-minus-circle-outline');
			$('#table_' + id).hide();
	    }
	);
}

function close_form(id, url)
{
	NProgress.start();
	$('#btn_' + id).prop('disabled', true);
	$('#div_add_' + id).slideUp('slow', function(){
		NProgress.done();
		$('#btn_' + id).prop('disabled', false);
		$('#div_add_' + id).html('');
		$('#table_' + id).slideDown();
		$('#btn_' + id).attr('onclick', 'show_form(\''+ id +'\', \''+ url +'\')');
		$('#text_' + id).text('Create '+ id);
		$('#icon_' + id).removeClass('mdi mdi-minus-circle-outline');
		$('#icon_' + id).addClass('mdi mdi-account-plus-outline');
		$('#breadcrumb_' + id).text('List');
	});
}
$(document).ready(function(){
  $('[data-toggle="tooltip"]').tooltip(); 
});