<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
$frontend = ['namespace' => 'Frontend'];

Route::group($frontend, function () {
	Route::get('/', 'HomeController@index');
	Route::get('/product', 'ProductController@index');
	Route::get('/product/detail/{id}', 'ProductController@detail');
	Route::get('/news/', 'NewsController@index');
	Route::get('/news/detail/{id}', 'NewsController@detail');
	Route::get('/teams/', 'TeamsController@index');
});	

Auth::routes();

Route::middleware(['auth'])->group(function (){
	$backend = ['prefix' => 'backend', 'namespace' => 'Backend'];

	Route::get('/get-menu', 'HomeController@get_menu');

	Route::group($backend, function () {
		Route::get('/home', 'HomeController@index')->name('home');

	    Route::group(['prefix' => 'product',], function () {
			Route::get('/', 'ProductController@index');
			Route::get('/create', 'ProductController@create');
			Route::get('/edit/{id}', 'ProductController@edit');
			Route::post('/store', 'ProductController@store');
			Route::post('/update/{id}', 'ProductController@update');
			Route::get('/destroy/{id}', 'ProductController@destroy');
	    });

	    Route::group(['prefix' => 'news',], function () {
			Route::get('/', 'NewsController@index');
			Route::get('/create', 'NewsController@create');
			Route::get('/edit/{id}', 'NewsController@edit');
			Route::post('/store', 'NewsController@store');
			Route::post('/update/{id}', 'NewsController@update');
			Route::get('/destroy/{id}', 'NewsController@destroy');
	    });

	    Route::group(['prefix' => 'slideshow',], function () {
			Route::get('/', 'SlideController@index');
			Route::get('/create', 'SlideController@create');
			Route::get('/edit/{id}', 'SlideController@edit');
			Route::post('/store', 'SlideController@store');
			Route::post('/update/{id}', 'SlideController@update');
			Route::get('/destroy/{id}', 'SlideController@destroy');
	    });

	    Route::group(['prefix' => 'teams',], function () {
			Route::get('/', 'TeamsController@index');
			Route::get('/create', 'TeamsController@create');
			Route::get('/edit/{id}', 'TeamsController@edit');
			Route::post('/store', 'TeamsController@store');
			Route::post('/update/{id}', 'TeamsController@update');
			Route::get('/destroy/{id}', 'TeamsController@destroy');
	    });

	    Route::group(['prefix' => 'testimonials',], function () {
			Route::get('/', 'TestimonialsController@index');
			Route::get('/create', 'TestimonialsController@create');
			Route::get('/edit/{id}', 'TestimonialsController@edit');
			Route::post('/store', 'TestimonialsController@store');
			Route::post('/update/{id}', 'TestimonialsController@update');
			Route::get('/destroy/{id}', 'TestimonialsController@destroy');
	    });

	    Route::group(['prefix' => 'flight',], function () {
			Route::get('/', 'FlightController@index');
			Route::get('/create', 'FlightController@create');
			Route::get('/edit/{id}', 'FlightController@edit');
			Route::post('/store', 'FlightController@store');
			Route::post('/update/{id}', 'FlightController@update');
			Route::get('/destroy/{id}', 'FlightController@destroy');
	    });

	    Route::group(['prefix' => 'status',], function () {
			Route::get('/', 'StatusController@index');
			Route::get('/create', 'StatusController@create');
			Route::get('/edit/{id}', 'StatusController@edit');
			Route::post('/store', 'StatusController@store');
			Route::post('/update/{id}', 'StatusController@update');
			Route::get('/destroy/{id}', 'StatusController@destroy');
	    });

	    Route::group(['prefix' => 'category',], function () {
			Route::get('/', 'TypeController@index');
			Route::get('/create', 'TypeController@create');
			Route::get('/edit/{id}', 'TypeController@edit');
			Route::post('/store', 'TypeController@store');
			Route::post('/update/{id}', 'TypeController@update');
			Route::get('/destroy/{id}', 'TypeController@destroy');
	    });
		
	});	
});
