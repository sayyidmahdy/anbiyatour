@extends('template.backend.index')
@section('content')
<div class="page-content-wrapper-inner max-width-1500">
	<div class="viewport-header">
		<nav aria-label="breadcrumb">
			<ol class="breadcrumb has-arrow">
				<li class="breadcrumb-item">
					<a href="#">Dashboard</a>
				</li>
				<li class="breadcrumb-item">
					<a href="#">Product</a>
				</li>
				<li class="breadcrumb-item active" aria-current="page" id="breadcrumb_crew">{{ ($act == 'add') ? 'Create' : 'Update' }}</li>
			</ol>
		</nav>
	</div>
	<div class="content-viewport">
		<div class="row">
			<div class="col-lg-12">
				<div class="grid">
					<div class="grid-body">
						<div class="item-wrapper">
							<form action="{{ ($act == 'add') ? url('backend/product/store') : url('backend/product/update/') . '/' . $data->id }}" method="POST" enctype="multipart/form-data">
								@csrf
								<div class="row">
									<div class="form-group col-lg-12">
										<label>Title</label>
										<input class="form-control" type="text" name="title" value="{{ ($act == 'add') ? '' : $data->title }}" required>
									</div>
									<div class="form-group col-lg-6">
										<label>Price</label>
										<input class="form-control" type="number" name="price" value="{{ ($act == 'add') ? '' : $data->price }}" required>
									</div>
									<div class="form-group col-lg-6">
										<label>Flight</label>
										<select class="form-control" name="flight">
											@foreach($flight as $row)
												@if($act == 'add')
													<option value="{{ $row->id }}">{{ $row->name }}</option>
												@else
													<option value="{{ $row->id }}" {{ ($data->flight == $row->id) ? 'selected' : '' }}>{{ $row->name }}</option>
												@endif
											@endforeach
										</select>
									</div>
									<div class="form-group col-lg-2">
										<label>Qty</label>
										<input class="form-control" type="text" name="qty" value="{{ ($act == 'add') ? '' : $data->qty }}" required>
									</div>
									<div class="form-group col-lg-4">
										<label>Category</label>
										<select class="form-control" name="type">
											@foreach($type as $row)
												@if($act == 'add')
													<option value="{{ $row->id }}">{{ $row->name }}</option>
												@else
													<option value="{{ $row->id }}" {{ ($data->type == $row->id) ? 'selected' : '' }}>{{ $row->name }}</option>
												@endif
											@endforeach
										</select>
									</div>
									<div class="form-group col-lg-6">
										<label for="inputPassword1">Image</label>
										<input type="file" name="image" {{ ($act == 'add') ? 'required' : ''}}>
									</div>
									<div class="form-group col-lg-12">
										<label for="inputPassword1">Description</label>
										<textarea class="form-control" name="description" cols="30" rows="10" required id="summernoteExample">{{ ($act == 'add') ? '' : $data->description }}</textarea>
									</div>
								</div>
								<button type="submit" class="btn btn-sm btn-primary">Save</button>
								<a href="{{ url('backend/product') }}" class="btn btn-sm btn-danger">Cancel</a>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection