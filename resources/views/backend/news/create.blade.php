@extends('template.backend.index')
@section('content')
<div class="page-content-wrapper-inner max-width-1500">
	<div class="viewport-header">
		<nav aria-label="breadcrumb">
			<ol class="breadcrumb has-arrow">
				<li class="breadcrumb-item">
					<a href="#">Dashboard</a>
				</li>
				<li class="breadcrumb-item">
					<a href="#">Content</a>
				</li>
				<li class="breadcrumb-item active" aria-current="page" id="breadcrumb_crew">{{ ($act == 'add') ? 'Create' : 'Update' }}</li>
			</ol>
		</nav>
	</div>
	<div class="content-viewport">
		<div class="row">
			<div class="col-lg-12">
				<div class="grid">
					<div class="grid-body">
						<div class="item-wrapper">
							<form action="{{ ($act == 'add') ? url('backend/news/store') : url('backend/news/update/') . '/' . $data->id }}" method="POST" enctype="multipart/form-data">
								@csrf
								<div class="form-group">
									<label for="inputEmail1">Title</label>
									<input class="form-control" type="text" name="title" placeholder="Title of Todo" value="{{ ($act == 'add') ? '' : $data->title }}" required>
								</div>
								<div class="form-group">
									<label for="inputPassword1">Description</label>
									<textarea class="form-control" name="content" cols="30" rows="10" required>{{ ($act == 'add') ? '' : $data->content }}</textarea>
								</div>
								<div class="form-group">
									<label for="inputPassword1">Image</label>
									<input type="file" name="image" {{ ($act == 'add') ? 'required' : ''}}>
								</div>
								<button type="submit" class="btn btn-sm btn-primary">Save</button>
								<a href="{{ url('backend/news') }}" class="btn btn-sm btn-danger">Cancel</a>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection