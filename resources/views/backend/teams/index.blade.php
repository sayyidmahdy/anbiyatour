@extends('template.backend.index')
@section('content')
	<div class="page-content-wrapper-inner max-width-1500" style="max-width: 100%">
		<div class="viewport-header">
			<nav aria-label="breadcrumb">
				<ol class="breadcrumb has-arrow">
					<li class="breadcrumb-item">
						<a href="#">Dashboard</a>
					</li>
					<li class="breadcrumb-item">
						<a href="#">Teams</a>
					</li>
					<li class="breadcrumb-item active" aria-current="page" id="breadcrumb_crew">List</li>
				</ol>
			</nav>
		</div>
		<div class="content-viewport">
			<div class="row">
				<div class="col-lg-12">
					<div class="grid">
						<div class="grid-body">
							<div class="item-wrapper">
								<a href="{{ url('backend/teams/create') }}" class="btn btn-info has-icon btn-rounded mb-4 pointer">
									<i class="mdi mdi-account-plus-outline"></i><b id="text_crew">Create</b>
								</a>
								<div class="table-responsive">
									<table id="complex-header-table" class="data-table table">
										<thead>
											<tr>
												<th width="3%">No</th>
												<th colspan="2">Full Name</th>
												<th>Position</th>
												<th>Status</th>
												<th>Tools</th>
											</tr>
										</thead>
										<tbody>
											@php $no = 1; @endphp
											@foreach($data as $row)
					                        <tr>
					                            <td> {{ $no++ }} </td>
					                            <td class="pr-0 pl-4"> <img class="profile-img img-sm" src="{{ asset('backend/image/teams') . '/' . $row->image }}"> </td>
					                            <td> {{ $row->name }} </td>
					                            <td> {{ $row->position }} </td>
					                            <td> 
					                            	<span class="badge badge-{{ ($row->status == 1) ? 'success' : 'danger' }}">{{ $row->status_name }}</span>
					                            </td>
					                            <td> 
					                                <a href="{{ url('backend/teams/edit/' . $row->slug) }}" class="btn btn-xs btn-primary">Edit</a> 
					                                <a href="#" class="btn btn-xs btn-danger" onclick="delete_event('{{ $row->id }}')">Delete</a> 
					                            </td>
					                        </tr>
					                        @endforeach
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection
@push('script')
	<script>

		function delete_event(id){
			var url = base_url + '/backend/teams/destroy/' + id;
			swal({
				text: 'Anda yakin akan menghapus data ini?', 
				type: 'warning',
				showCancelButton: true
			}).then(result => {
			  	if (result.value) {
					$.ajax({
						type: "GET",
						url: url,
						success: function(data){
						  	window.location.reload();
						},
						error: function(){
						  	showErrorToast('bottom-right', 'Hapus data teams gagal!');
						}
					});
			  	}
			})
		}
	</script>
@endpush