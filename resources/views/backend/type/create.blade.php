@extends('template.backend.index')
@section('content')
<div class="page-content-wrapper-inner max-width-1500">
	<div class="viewport-header">
		<nav aria-label="breadcrumb">
			<ol class="breadcrumb has-arrow">
				<li class="breadcrumb-item">
					<a href="#">Dashboard</a>
				</li>
				<li class="breadcrumb-item">
					<a href="#">Mster Category</a>
				</li>
				<li class="breadcrumb-item active" aria-current="page" id="breadcrumb_crew">{{ ($act == 'add') ? 'Create' : 'Update' }}</li>
			</ol>
		</nav>
	</div>
	<div class="content-viewport">
		<div class="row">
			<div class="col-lg-12">
				<div class="grid">
					<div class="grid-body">
						<div class="item-wrapper">
							<form action="{{ ($act == 'add') ? url('backend/category/store') : url('backend/category/update/') . '/' . $data->id }}" method="POST" enctype="multipart/form-data">
								@csrf
								<div class="row">
									<div class="form-group col-lg-6">
										<label>Name</label>
										<input class="form-control" type="text" name="name" value="{{ ($act == 'add') ? '' : $data->name }}" required>
									</div>
								</div>
								<button type="submit" class="btn btn-sm btn-primary">Save</button>
								<a href="{{ url('backend/category') }}" class="btn btn-sm btn-danger">Cancel</a>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection