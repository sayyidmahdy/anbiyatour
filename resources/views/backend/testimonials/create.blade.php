@extends('template.backend.index')
@section('content')
<div class="page-content-wrapper-inner max-width-1500">
	<div class="viewport-header">
		<nav aria-label="breadcrumb">
			<ol class="breadcrumb has-arrow">
				<li class="breadcrumb-item">
					<a href="#">Dashboard</a>
				</li>
				<li class="breadcrumb-item">
					<a href="#">Testimonials</a>
				</li>
				<li class="breadcrumb-item active" aria-current="page" id="breadcrumb_crew">{{ ($act == 'add') ? 'Create' : 'Update' }}</li>
			</ol>
		</nav>
	</div>
	<div class="content-viewport">
		<div class="row">
			<div class="col-lg-12">
				<div class="grid">
					<div class="grid-body">
						<div class="item-wrapper">
							<form action="{{ ($act == 'add') ? url('backend/testimonials/store') : url('backend/testimonials/update/') . '/' . $data->id }}" method="POST" enctype="multipart/form-data">
								@csrf
								<div class="row">
									<div class="form-group col-lg-6">
										<label>User</label>
										<input class="form-control" type="text" name="user" value="{{ ($act == 'add') ? '' : $data->user }}" required>
									</div>
									<div class="form-group col-lg-6">
										<label>Company</label>
										<input class="form-control" type="text" name="company" value="{{ ($act == 'add') ? '' : $data->company }}" required>
									</div>
									<div class="form-group col-lg-12">
										<label>Image</label>
										<input type="file" name="image" {{ ($act == 'add') ? 'required' : ''}}>
									</div>
								</div>
								<button type="submit" class="btn btn-sm btn-primary">Save</button>
								<a href="{{ url('backend/testimonials') }}" class="btn btn-sm btn-danger">Cancel</a>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection