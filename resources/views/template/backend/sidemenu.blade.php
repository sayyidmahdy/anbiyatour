<div class="sidebar">
	<ul class="navigation-menu">
		<li class="nav-category-divider">MAIN</li>
		<!-- <li>
			<a href="{{ url('backend/home') }}">
				<span class="link-title">Dashboard</span>
				<i class="mdi mdi-gauge link-icon"></i>
			</a>
		</li> -->
		<li>
			<a href="{{ url('backend/news') }}">
				<span class="link-title">News</span>
				<i class="mdi mdi-newspaper link-icon"></i>
			</a>
		</li>
		<li>
			<a href="{{ url('backend/product') }}">
				<span class="link-title">Product</span>
				<i class="mdi mdi-briefcase link-icon"></i>
			</a>
		</li>
		<li>
			<a href="{{ url('backend/slideshow') }}">
				<span class="link-title">Slider</span>
				<i class="mdi mdi-apps link-icon"></i>
			</a>
		</li>
		<li>
			<a href="{{ url('backend/teams') }}">
				<span class="link-title">Teams</span>
				<i class="mdi mdi-account link-icon"></i>
			</a>
		</li>
		<li>
			<a href="{{ url('backend/testimonials') }}">
				<span class="link-title">Testimonials</span>
				<i class="mdi mdi-assistant link-icon"></i>
			</a>
		</li>
		<li>
			<a href="#sample-pages" data-toggle="collapse" aria-expanded="false">
				<span class="link-title">Setting</span>
				<i class="mdi mdi-flask link-icon"></i>
			</a>
			<ul class="collapse navigation-submenu" id="sample-pages">
				<li>
					<li>
						<a href="{{ url('backend/category') }}">Master Category</a>
					</li>
					<li>
						<a href="{{ url('backend/flight') }}">Master Partner</a>
					</li>
					<li>
						<a href="{{ url('backend/status') }}">Master Status</a>
					</li>
				</li>
			</ul>
		</li>
	</ul>
	<div class="sidebar_footer">
		<div class="user-account">
			<a class="user-profile-item" href="#"><i class="mdi mdi-account"></i> Profile</a>
			<a class="user-profile-item" href="#"><i class="mdi mdi-settings"></i> Account Settings</a>
			<a class="btn btn-primary btn-logout" href="{{ route('logout') }}"onclick="event.preventDefault();document.getElementById('logout-form').submit();">Logout</a>
			<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
            </form>
		</div>
		<div class="btn-group admin-access-level">
			<div class="avatar">
				<img class="profile-img" src="http://www.placehold.it/50x50" alt="">
			</div>
			<div class="user-type-wrapper">
				<p class="user_name">{{ Auth::user()->name }}</p>
				<div class="d-flex align-items-center">
					<div class="status-indicator small rounded-indicator bg-success"></div>
					<small class="user_access_level">Admin</small>
				</div>
			</div>
			<i class="arrow mdi mdi-chevron-right"></i>
		</div>
	</div>
</div>