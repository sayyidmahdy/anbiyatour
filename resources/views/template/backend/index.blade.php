<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Anbiya Tour</title>
    <!-- plugins:css -->
    <link rel="stylesheet" href="{{ asset('assets/backend/vendors/iconfonts/mdi/css/materialdesignicons.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/backend/vendors/css/vendor.addons.css') }}">
    <!-- endinject -->
    <!-- vendor css for this page -->
    <link rel="stylesheet" href="{{ asset('assets/backend/vendors/iconfonts/flag-icon-css/css/flag-icon.min.css') }}">
    <!-- End vendor css for this page -->
    <!-- inject:css -->
    <link rel="stylesheet" href="{{ asset('assets/backend/css/shared/style.css') }}">
    <!-- endinject -->
    <!-- Layout style -->
    <link rel="stylesheet" href="{{ asset('assets/backend/css/demo_1/style.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/backend/vendors/iconfonts/font-awesome/css/font-awesome.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/backend/plugin/nProgress/nprogress.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/backend/vendors/summernote/dist/summernote-lite.css')}}">
    <!-- Layout style -->
    <link rel="shortcut icon" href="{{ asset('assets/img/favicon.jpeg') }}" />
  </head>
  <body class="header-fixed">
    @include('template.backend.header')

    <div class="page-body">
      @include('template.backend.sidemenu')
      
      <div class="page-content-wrapper">

        @yield('content')
        
        @include('template.backend.footer')

      </div>
    </div>
    @stack('script')
    <!--page body ends -->
    <!-- SCRIPT LOADING START FORM HERE /////////////-->
    <!-- plugins:js -->
    <script src="{{ asset('assets/backend/vendors/js/core.js') }}"></script>
    <script src="{{ asset('assets/backend/vendors/js/vendor.addons.js') }}"></script>
    <!-- endinject -->
    <!-- Vendor Js For This Page Ends-->
    <script src="{{ asset('assets/backend/vendors/chartjs/Chart.min.js') }}"></script>
    <!-- Vendor Js For This Page Ends-->
    <!-- build:js -->
    <script src="{{ asset('assets/backend/js/template.js') }}"></script>
    <script src="{{ asset('assets/backend/js/dashboard.js') }}"></script>
    <script src="{{ asset('assets/backend/js/data-table.js') }}"></script>
    <script src="{{ asset('assets/backend/js/forms/validation.js') }}"></script>
    <script src="{{ asset('assets/backend/js/forms/form_elements.js') }}"></script>
    <script src="{{ asset('assets/backend/js/form_validator.js') }}"></script>
    <script src="{{ asset('assets/backend/js/modal-function.js') }}"></script>
    <script src="{{ asset('assets/backend/js/custom.js') }}"></script>
    <script src="{{ asset('assets/backend/js/notifications.js') }}"></script>
    <script src="{{ asset('assets/js/sweetalert.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/backend/plugin/nProgress/nprogress.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/js/bootstrap-filestyle.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/backend/vendors/summernote/dist/summernote-lite.min.js')}}"></script>

    <!-- endbuild -->
    <script type="text/javascript">
      var base_url = "{{ url('/') }}";
      NProgress.start();
      NProgress.set(0.4);
      //Increment 
      var interval = setInterval(function() { NProgress.inc(); }, 1000);
      $(":file").filestyle({btnClass: "btn-primary btn-sm"});

      $(document).ready(function(){
          NProgress.done();
          clearInterval(interval);
          @if(Session::has('message'))
              @php
                  $flashmsg = explode('|', Session::get("message"));
                  $type = $flashmsg[0];
                  $msg  = $flashmsg[1];
              @endphp

              var type = '{{ $type }}';
              var msg  = '{{ $msg }}';

              if (type == 'success') {
                showSuccessToast('bottom-right', msg);
              } else {
                showErrorToast('bottom-right', msg)
              }

          @endif

          $('#summernoteExample').summernote({
                height: 300,
                tabsize: 2
            });
      });
    </script>
    
  </body>
</html>