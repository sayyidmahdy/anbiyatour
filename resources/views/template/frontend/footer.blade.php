<footer id="footer" style="background: #101936; !important">
	<div class="container">
		<div class="row">
			<div class="col-sm-4">
				<!-- <div>
					<h3 class="title m_title text-white "></h3>
					<div class="sbs">
						<ul class="menu">
							<li><a href="#">FAQ</a></li>
							<li><a href="#">&nbsp;</a></li>
							<li><a href="#">Syarat & Ketentuan</a></li>
						</ul>
					</div>
				</div> -->
				<div class="newsletter-signup">
					<h3 class="title m_title text-white ">CONTACT US</h3>
					<div class="contact-details">
						<p class="text-white"> 
							Head Office: <strong>(021) 84301175 </strong>
							<br> Respresentative: <strong><a href="https://api.whatsapp.com/send?phone=6281585232346&text=Ada%20yang%20bisa%20di%20bantu?" target="_blank">081585232346</a></strong>
							<br>Customer Service: <strong><a href="https://api.whatsapp.com/send?phone=6287777711511&text=Ada%20yang%20bisa%20di%20bantu?" target="_blank">087777711511</a></strong>
							<br>Email: anbiyatour@gmail.com
						</p>
					</div>
				</div><!-- end newsletter-signup -->
			</div>
			<!--/ col-sm-5 -->
			<div class="col-sm-4">
			</div>
			<div class="col-sm-4">
				<div>
					<h3 class="title m_title text-white ">HEAD OFFICE</h3>
					<div class="">
						<p class="text-white ">

						Ruko Kranggan Permai, Blok RT16 Jl. Alternatif Cibubur No.23, Jatisampurna, Bekasi City, West Java 17435</p>
						<p class="text-white "><a href="https://www.google.com/maps/place/Anbiya+Tour+%26+Travel/@-6.3749893,106.9160559,15z/data=!4m2!3m1!1s0x0:0xc4b1bee1a3ef62e9?sa=X&ved=2ahUKEwjHmr7__5zkAhVIvo8KHaxNBrIQ_BIwE3oECA0QCA" target="_blank"><i class="icon-map-marker icon-white"></i> Open in Google Maps</a></p>
					</div>
				</div>
			</div>
			<!-- col-sm-4 -->

			<!-- <div class="col-sm-4">
				<div>
					<h3 class="title m_title text-white ">RESPRESENTATIVE</h3>
					<div class="">
						<p class="text-white ">
						Jl. Sukarela V no.18 RT.002 RW.003. Paninggilan Ciledug, Kota Tangerang 15153 </p>
						<p class="text-white "><a href="https://goo.gl/maps/nW9vUTHWsgF4hw6s5" target="_blank"><i class="icon-map-marker icon-white"></i> Open in Google Maps</a></p>
					</div>
				</div>
			</div> -->
			<!--/ col-sm-3 -->
		</div>


		<div class="row">
			<div class="col-sm-12">
				<div class="bottom clearfix">
					<!-- social-icons -->
					<ul class="social-icons sc--clean clearfix">
						<li class="title text-white ">GET SOCIAL</li>
						<li><a href="https://www.facebook.com/anbiyatourandtravel/" target="_blank" title="Facebook"><img src="{{ asset('assets/img/facebook.png') }}" style="max-width: 20px;"></a></li>
						<li><a href="#" target="_blank" title="Twitter"><img src="{{ asset('assets/img/twitter.png') }}" style="max-width: 20px;"></a></li>
						<li><a href="https://www.instagram.com/anbiyatourandtravel/?hl=id" target="_blank" title="Instagram"><img src="{{ asset('assets/img/instagram.png') }}" style="max-width: 20px;"></a></li>
					</ul>
					<!--/ social-icons -->

					<!-- copyright -->
					<div class="copyright">
						<a href="index.html">
							<!-- <img src="{{ asset('assets/img/Anbiyalogo.png') }}"> -->
						</a>
						<p class="text-white ">© @php echo date('Y') @endphp Anbiya Tour & Travel All rights reserved.</p>
					</div>
					<!--/ copyright -->
				</div>
				<!--/ bottom -->
			</div>
			<!--/ col-sm-12 -->
		</div>
		<!--/ row -->
	</div>
	<!--/ container -->
</footer>