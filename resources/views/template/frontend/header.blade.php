@php
	$menu = DB::table('m_types')->get();
@endphp

<header id="header" class="site-header style1 cta_button">
	<!-- header bg -->
	<div class="kl-header-bg"></div>
	<!--/ header bg -->

	<!-- siteheader-container -->
	<div class="container siteheader-container">
		<!-- top-header -->
		<div class="kl-top-header clearfix">
			<div class="header-links-container ">
				<ul class="topnav navRight topnav">
					<li>
						<label for="support_p" class="spanel-label">
                    		<i class="glyphicon glyphicon-info-sign icon-white support-info visible-xs xs-icon"></i>
							<!-- <span class="hidden-xs">SUPPORT</span> -->
						</label>
					</li>

					<li>
						<a class="popup-with-form" href="#login_panel">
							<i class="glyphicon glyphicon-log-in visible-xs xs-icon"></i>
							<!-- <span class="hidden-xs">LOGIN</span> -->
						</a>
					</li>			
				</ul>
			</div>

			<div class="header-leftside-container ">
				<ul class="social-icons sc--clean topnav navRight">
					<!-- <li><a href="#" target="_self" class="icon-facebook" title="Facebook"></a></li>
					<li><a href="#" target="_self" class="icon-twitter" title="Twitter"></a></li>	
					<li><a href="#" target="_self" class="icon-pinterest" title="Pinterest"></a></li>
					<li><a href="#" target="_blank" class="icon-gplus" title="Google Plus"></a></li> -->
				</ul>

				<div class="clearfix visible-xxs">
				</div>

				<!-- <span class="kl-header-toptext">QUESTIONS? CALL: <a href="#" class="fw-bold">0900 800 900</a></span> -->
			</div>
		</div>
		<!--/ top-header -->
		
		<!-- separator -->
		<div class="separator"></div>
		<!--/ separator -->

		<!-- left side -->
		<!-- logo container-->
		<div class="logo-container hasInfoCard logosize--yes">
			<!-- Logo -->
			<h1 class="site-logo logo" id="logo">
				<a href="{{ url('/') }}" title="">
					<img src="{{ asset('assets/img/favicon.jpeg') }}" class="logo-img" />
					<!-- <img src="{{ asset('assets/img/Anbiyalogo.png') }}" class="logo-img" /> -->
				</a>
			</h1>
			<!--/ Logo -->

			<!-- InfoCard -->
			<div id="infocard" class="logo-infocard">
				<div class="custom">
					<div class="row">
						<div class="col-sm-5">
							<p style="text-align: center;">
								<img src="{{ asset('assets/img/favicon.jpeg') }}" class="" alt="Kallyas" title="Kallyas" />
								<!-- <img src="{{ asset('assets/img/favicon.png') }}" class="" alt="Kallyas" title="Kallyas" /> -->
							</p>
						</div>
						<!--/ col-sm-5 -->

						<div class="col-sm-7">
							<div class="custom contact-details">
								<p>
									<strong>(021) 84301175</strong><br>
									Email:&nbsp;<a href="mailto:sales@yourwebsite.com">sales@yourwebsite.com</a>
								</p>
								<p>
									Anbiyatour<br>
									Ruko Kranggan Permai, Blok RT16 Jl. Alternatif Cibubur No.23, Jatisampurna, Bekasi City, West Java 17435
								</p>
								<a href="https://www.google.com/maps/place/Anbiya+Tour+%26+Travel/@-6.3749893,106.9160559,15z/data=!4m2!3m1!1s0x0:0xc4b1bee1a3ef62e9?sa=X&ved=2ahUKEwjHmr7__5zkAhVIvo8KHaxNBrIQ_BIwE3oECA0QCA" class="map-link" target="_blank" title="">
									<span class="glyphicon glyphicon-map-marker icon-white"></span>
									<span>Open in Google Maps</span>
								</a>
							</div>
							<div style="height:20px;">
							</div>
							<!-- Social links clean style -->
							<!-- <ul class="social-icons sc--clean">
								<li><a href="#" target="_self" class="icon-twitter" title="Twitter"></a></li>
								<li><a href="#" target="_self" class="icon-facebook" title="Facebook"></a></li>
								<li><a href="#" target="_self" class="icon-dribbble" title="Dribbble"></a></li>
								<li><a href="#" target="_blank" class="icon-google" title="Google Plus"></a></li>
							</ul> -->
							<!--/ Social links clean style -->
						</div>
						<!--/ col-sm-7 -->
					</div>
					<!--/ row -->
				</div>
				<!--/ custom -->
			</div>
			<!--/ InfoCard -->

		</div>
		<!--/ logo container-->

		<!-- separator -->
		<div class="separator visible-xxs"></div>
		<!--/ separator -->

		<!-- responsive menu trigger -->
		<div id="zn-res-menuwrapper">
			<a href="#" class="zn-res-trigger zn-header-icon"></a>
		</div>
		<!--/ responsive menu trigger -->

		<!-- main menu -->
		<div id="main-menu" class="main-nav zn_mega_wrapper" style="margin-top: 15px">
			<ul id="menu-main-menu" class="main-menu zn_mega_menu">
				<li><a href="{{ url('/') }}">HOMEPAGES</a></li>
				<li><a href="{{ url('teams') }}">TEAMS</a></li>

				@foreach($menu as $row_menu)
					<li><a href="{{ url('product?catalog=' . $row_menu->slug) }}">{{ strtoupper($row_menu->name) }}</a></li>
				@endforeach
				<li><a href="{{ url('news') }}">NEWS</a></li>
				<li class="active" style="color: #c2a73e;"><a href="https://api.whatsapp.com/send?phone=6287777711511&text=Ada%20yang%20bisa%20di%20bantu?" target="_blank">DAFTAR</a></li>
				<!-- <li class="menu-item-has-children"><a href="shop-landing-page-default.html">Product</a>
					<ul class="sub-menu clearfix">
					</ul>
				</li> -->
			</ul>
		</div>
		<!--/ main menu -->

		<!-- right side -->	
		<!-- Call to action ribbon Free Quote -->
		<!-- <a href="#" id="ctabutton" class="ctabutton kl-cta-ribbon" title="GET A FREE QUOTE" target="_self"><strong>REGISTER</strong>NOW<svg version="1.1" class="trisvg" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" preserveaspectratio="none" width="14px" height="5px" viewbox="0 0 14.017 5.006" enable-background="new 0 0 14.017 5.006" xml:space="preserve"><path fill-rule="evenodd" clip-rule="evenodd" d="M14.016,0L7.008,5.006L0,0H14.016z"></path></svg></a> -->
		<!--/ Call to action ribbon Free Quote -->


	</div>
	<!--/ siteheader-container -->
</header>