<!doctype html>
<html class="no-js" lang="en-US">
<head>

	<!-- meta -->
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />

	<!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
	<meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1">

	<!-- Uncomment the meta tags you are going to use! Be relevant and don't spam! -->

	<meta name="keywords" content="premium html template, unique premium template, multipurpose template" />
	<meta name="description" content="Kallyas is an ultra-premium, responsive theme built for todays websites. Create your website, fast.">

	<!-- Title -->
	<title>Anbiya Tour</title>


	<!--  Desktop Favicons  -->
	<link rel="icon" type="image/png" href="{{ asset('assets/img/favicon.jpeg') }}" sizes="16x16">
	<!-- <link rel="icon" type="image/png" href="images/favicons/favicon-32x32.png" sizes="32x32"> -->
	<!-- <link rel="icon" type="image/png" href="images/favicons/favicon-96x96.png" sizes="96x96"> -->

	<!-- Google Fonts CSS Stylesheet // More here http://www.google.com/fonts#UsePlace:use/Collection:Open+Sans -->
	<link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400italic,400,600,600italic,700,800,800italic" rel="stylesheet" type="text/css">
	<link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>

	<!-- ***** Boostrap Custom / Addons Stylesheets ***** -->
	<link rel="stylesheet" href="{{ asset('assets/frontend/css/bootstrap.css') }}" type="text/css" media="all">

	<!-- Required CSS file for IOS Slider element  -->
	<link rel="stylesheet" href="{{ asset('assets/frontend/css/sliders/ios/style.css') }}" type="text/css" media="all">

	<!-- ***** Main + Responsive & Base sizing CSS Stylesheet ***** -->
	<link rel="stylesheet" href="{{ asset('assets/frontend/css/template.css') }}" type="text/css" media="all">
	<link rel="stylesheet" href="{{ asset('assets/frontend/css/responsive.css') }}" type="text/css" media="all">
	<link rel="stylesheet" href="{{ asset('assets/frontend/css/base-sizing.css') }}" type="text/css" media="all">

	<!-- Custom CSS Stylesheet (where you should add your own css rules) -->
	<link rel="stylesheet" href="{{ asset('assets/frontend/css/niches/custom-travel.css') }}" type="text/css" />

	<!-- Modernizr Library -->
	<script type="text/javascript" src="{{ asset('assets/frontend/js/modernizr.min.js') }}"></script>

	<!-- jQuery Library -->
	<script type="text/javascript" src="{{ asset('assets/frontend/js/jquery.js') }}"></script>

</head>

<body class="">
	

	<!-- Support Panel -->
	<input type="checkbox" id="support_p" class="panel-checkbox">
	<div class="support_panel">
		<div class="support-close-inner">
			<label for="support_p" class="spanel-label inner">
				<span class="support-panel-close">×</span>
			</label>
		</div>	
		<div class="container">		
			<div class="row">
				<div class="col-sm-9">
					<!-- Title -->
					<h4 class="m_title">HOW TO SHOP</h4>

					<!-- Content - how to shop steps -->
					<div class="m_content how_to_shop">
						<div class="row">
							<div class="col-sm-4">
								<span class="number">1</span> Login or create new account.
							</div>
							<!--/ col-sm-4 -->

							<div class="col-sm-4">
								<span class="number">2</span> Review your order.
							</div>
							<!--/ col-sm-4 -->

							<div class="col-sm-4">
								<span class="number">3</span> Payment &amp; <strong>FREE</strong> shipment
							</div>
							<!--/ col-sm-4 -->
						</div>
						<!--/ row -->

						<p>If you still have problems, please let us know, by sending an email to support@website.com . Thank you!</p>
					</div>
					<!--/ Content - how to shop steps -->
				</div>
				<!--/ col-sm-9 -->

				<div class="col-sm-3">
					<!-- Title -->
					<h4 class="m_title">SHOWROOM HOURS</h4>

					<!-- Content -->
					<div class="m_content">
						Mon-Fri 9:00AM - 6:00AM<br>
						Sat - 9:00AM-5:00PM<br>
						Sundays by appointment only!
					</div>
					<!--/ Content -->
				</div>
				<!--/ col-sm-3 -->
			</div>
			<!--/ row -->
		</div>
		<!--/ container -->
	</div>
	<!--/ Support Panel -->
	

	<!-- Page Wrapper -->
	<div id="page_wrapper">
		<!-- Header style 1 -->
		@include('template.frontend.header')
		<!-- / Header style 1 -->


		@yield('content')
		<!--/ Content section -->


		<!-- Footer - Default Style -->
		@include('template.frontend.footer')
		<!--/ Footer - Default Style -->
	</div>
	<!--/ Page Wrapper -->


	<!-- Login Panel content -->
	@include('template.frontend.login')
	<!--/ Login Panel content -->
	

	<!-- ToTop trigger -->
	<a href="#" id="totop">TOP</a>
	<!--/ ToTop trigger -->


	


	<!-- JS FILES // These should be loaded in every page -->
	<script type="text/javascript" src="{{ asset('assets/frontend/js/bootstrap.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('assets/frontend/js/kl-plugins.js') }}"></script>

	<!-- JS FILES // Loaded on this page -->
	<!-- Required js script for animateme scroll effect for slideshow  -->
	<script type="text/javascript" src="{{ asset('assets/frontend/js/plugins/scrollme/jquery.scrollme.js') }}"></script>

	<!-- Required js script for iOS slider element -->
	<script type="text/javascript" src="{{ asset('assets/frontend/js/plugins/_sliders/ios/jquery.iosslider.min.js') }}"></script>

	<!-- Required js trigger for iOS Slider element -->
	<script type="text/javascript" src="{{ asset('assets/frontend/js/trigger/slider/ios/kl-ios-slider.js') }}"></script>

	<!-- Custom Kallyas JS codes -->
	<script type="text/javascript" src="{{ asset('assets/frontend/js/kl-scripts.js') }}"></script>

	<!-- Custom user JS codes -->
	<script type="text/javascript" src="{{ asset('assets/frontend/js/kl-custom.js') }}"></script>

	<!-- JS FILES // Loaded on this page -->

	<!-- CarouFredSel required js script for Recent work carousel element -->
	<script type="text/javascript" src="{{ asset('assets/frontend/js/plugins/_sliders/caroufredsel/jquery.carouFredSel-packed.js') }}"></script>

	<!-- Required js trigger for Recent Work 1 Carousel element -->
	<script type="text/javascript" src="{{ asset('assets/frontend/js/trigger/kl-recent-work-carousel.js') }}"></script>

	<!-- Required js trigger for Recent Work #3 Carousel -->
	<script type="text/javascript" src="{{ asset('assets/frontend/js/trigger/kl-recent-work-carousel3.js') }}"></script>
	<!-- <script type="text/javascript" src="{{ asset('assets/frontend/js/plugins/jquery-ui-1.10.3.custom.min.js')}}"></script> -->

	<!-- Required js trigger for Screenshot Box Carousel -->
	<script type="text/javascript" src="{{ asset('assets/frontend/js/trigger/kl-screenshot-box.js') }}"></script>

	<!-- Required js trigger for Partners Carousel -->
	<script type="text/javascript" src="{{ asset('assets/frontend/js/trigger/kl-partners-carousel.js') }}"></script>	
	
	

	<!-- Google Analytics: change UA-XXXXX-X to be your site's ID.
	<script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	  ga('create', 'UA-XXXXX-X', 'auto');
	  ga('send', 'pageview');
	</script>
	-->

</body>
</html>