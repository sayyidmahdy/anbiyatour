@extends('template.frontend.index')
@section('content')
<!-- Page sub-header + Bottom mask style 3 -->
<div id="page_header" class="page-subheader site-subheader-cst uh_flat_dark_blue maskcontainer--mask3">
	<div class="bgback">
	</div>

	<!-- Animated Sparkles -->
	<div class="th-sparkles"></div>
	<!--/ Animated Sparkles -->

	<!-- Background source -->
	<div class="kl-bg-source">
		<!-- Background image -->
		<div class="kl-bg-source__bgimage" style="background-image:url({{ asset('assets/frontend/images/_niches/travel/plane.jpg')}}); background-repeat:no-repeat; background-attachment:scroll; background-position-x:center; background-position-y:center; background-size:cover">
		</div>
		<!--/ Background image -->

		<!-- Gradient overlay -->
		<div class="kl-bg-source__overlay" style="background:rgba(0,94,176,0.8); background: -moz-linear-gradient(left, rgba(0,94,176,0.8) 0%, rgba(0,202,255,0.9) 100%); background: -webkit-gradient(linear, left top, right top, color-stop(0%,rgba(0,94,176,0.8)), color-stop(100%,rgba(0,202,255,0.9))); background: -webkit-linear-gradient(left, rgba(0,94,176,0.8) 0%,rgba(0,202,255,0.9) 100%); background: -o-linear-gradient(left, rgba(0,94,176,0.8) 0%,rgba(0,202,255,0.9) 100%); background: -ms-linear-gradient(left, rgba(0,94,176,0.8) 0%,rgba(0,202,255,0.9) 100%); background: linear-gradient(to right, rgba(0,94,176,0.8) 0%,rgba(0,202,255,0.9) 100%); ">
		</div>
		<!--/ Gradient overlay -->
	</div>
	<!--/ Background source -->

	<!-- Sub-Header content wrapper -->
	<div class="ph-content-wrap">
		<div class="ph-content-v-center">
			<div class="container">
				<div class="row">
					<div class="col-sm-6">
						<!-- Breadcrumbs -->
						<ul class="breadcrumbs fixclear">
							<li><a href="{{ url('/') }}">Home</a></li>
							<li><a href="{{ url('/news/') }}">News</a></li>
						</ul>
						<!--/ Breadcrumbs -->

						<div class="clearfix"></div>
					</div>
					
					<!--/ col-sm-6 -->
				</div>
				<!--/ row -->
			</div>
			<!--/ container -->
		</div>
		<!--/ .ph-content-v-center -->
	</div>
	<!--/ Sub-Header content wrapper -->

	<!-- Bottom mask style 3 -->
	<div class="kl-bottommask kl-bottommask--mask3">
		<svg width="5000px" height="57px" class="svgmask " viewBox="0 0 5000 57" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
			<defs>
				<filter x="-50%" y="-50%" width="200%" height="200%" filterUnits="objectBoundingBox" id="filter-mask3">
					<feOffset dx="0" dy="3" in="SourceAlpha" result="shadowOffsetInner1"></feOffset>
					<feGaussianBlur stdDeviation="2" in="shadowOffsetInner1" result="shadowBlurInner1"></feGaussianBlur>
					<feComposite in="shadowBlurInner1" in2="SourceAlpha" operator="arithmetic" k2="-1" k3="1" result="shadowInnerInner1"></feComposite>
					<feColorMatrix values="0 0 0 0 0   0 0 0 0 0   0 0 0 0 0  0 0 0 0.4 0" in="shadowInnerInner1" type="matrix" result="shadowMatrixInner1"></feColorMatrix>
					<feMerge>
						<feMergeNode in="SourceGraphic"></feMergeNode>
						<feMergeNode in="shadowMatrixInner1"></feMergeNode>
					</feMerge>
				</filter>
			</defs>
			<path d="M9.09383679e-13,57.0005249 L9.09383679e-13,34.0075249 L2418,34.0075249 L2434,34.0075249 C2434,34.0075249 2441.89,33.2585249 2448,31.0245249 C2454.11,28.7905249 2479,11.0005249 2479,11.0005249 L2492,2.00052487 C2492,2.00052487 2495.121,-0.0374751261 2500,0.000524873861 C2505.267,-0.0294751261 2508,2.00052487 2508,2.00052487 L2521,11.0005249 C2521,11.0005249 2545.89,28.7905249 2552,31.0245249 C2558.11,33.2585249 2566,34.0075249 2566,34.0075249 L2582,34.0075249 L5000,34.0075249 L5000,57.0005249 L2500,57.0005249 L1148,57.0005249 L9.09383679e-13,57.0005249 Z" class="bmask-bgfill" filter="url(#filter-mask3)" fill="#f5f5f5"></path>
		</svg>
	    <i class="glyphicon glyphicon-chevron-down"></i>
	</div>
	<!--/ Bottom mask style 3 -->
</div>
<!--/ Page sub-header + Bottom mask style 3 -->
<section class="zn_section eluidf9b7d5d6     section-sidemargins    section--no " id="eluidf9b7d5d6">
	<div class="zn_section_size container zn-section-height--auto zn-section-content_algn--top ">
		<div class="row ">
			<div class="col-md-12 col-sm-12">
				<!-- Title element custom -->
				<div class="kl-title-block clearfix text-center tbk-icon-pos--after-title pbottom-50">
					<!-- Title with custom font size, line height  and thin style -->
					<h3 class="tbk__title fs-32 lh-52 fw-thin">THE TEAM</h3>
					<!--/ Title with custom font size, line height and thin style -->

					<!-- Title bottom symbol -->
					<div class="symbol-line">
						<span class="kl-icon tcolor icon-spinner10"></span>
					</div>
					<!--/ Title bottom symbol -->

					<!-- Sub-title with custom font size -->
					<h4 class="tbk__subtitle fs-14">APPROPRIATELY MORPH TECHNICALLY SOUND LEADERSHIP SKILLS</h4>
					<!--/ Sub-title with custom font size -->
				</div>
				<!--/ Title element custom -->
			</div>
			@foreach($teams as $team)
			<div class="eluid300c8086            col-md-4 col-sm-4   znColumnElement" id="eluid300c8086">
				<div class="znColumnElement-innerWrapper-eluid300c8086 znColumnElement-innerWrapper znColumnElement-innerWrapper--valign-top znColumnElement-innerWrapper--halign-left ">
					<div class="znColumnElement-innerContent">
						<div onclick="" class="team_member team-member u-trans-all-2s eluida784bfb3  teambox--light element-scheme--light teambox-style--hover">
							<a href="#" class="grayHover team-member-link" target="_blank" rel="noopener" itemprop="url">
								<img class="team-member-img" src="https://demo.kallyas.net/wp-content/uploads/2015/08/member-01-370x370_c.jpg" width="370" height="370" alt="" title="">
							</a>
							<h4 class="team-member-name" itemscope="itemscope" itemtype="https://schema.org/Person">Michael Jonas</h4>
							<h6 class="team-member-pos">CEO / Founder</h6>
							<div class="details team-member-details">
								<div class="desc team-member-desc">
									<p>Synergistically foster extensive technologies via extensible quality vectors. Synergistically unleash client-centric niche markets.</p>
								</div>
								<ul class="social-icons sc--colored fixclear">
									<li class="social-icons-li">
										<a data-zniconfam="kl-social-icons" data-zn_icon="" href="#" target="_blank" title="Facebook" class="social-icons-item sctb-icon-ue83f"></a>
									</li>
									<li class="social-icons-li">
										<a data-zniconfam="kl-social-icons" data-zn_icon="" href="#" target="_blank" title="Twitter" class="social-icons-item sctb-icon-ue82f"></a>
									</li>
									<li class="social-icons-li">
										<a data-zniconfam="kl-social-icons" data-zn_icon="" href="#" target="_blank" title="Google Plus" class="social-icons-item sctb-icon-ue808"></a>
									</li>
									<li class="social-icons-li">
										<a data-zniconfam="kl-social-icons" data-zn_icon="" href="#" target="_blank" title="LinkedIn" class="social-icons-item sctb-icon-ue828"></a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
			@endforeach
		</div>
	</div>
</section>

<section class="hg_section bg-white pt-80 pb-80">
	<div class="container">
		<div class="row">
			
			<!--/ col-md-12 col-sm-12 -->

			<div class="col-sm-12">
				<div class="team-carousel caroufredsel stg-slim-arrows" data-setup='{ "navigation": true, "pagination": false, "width": "variable", "items_width": "360", "items_height": "variable", "auto_duration": 9000, "items": {"min":1, "max":4} }'>
					<ul class="slides">
						@foreach($teams as $team)
						<li>
							<div class="team-member tm-hover text-center">
								<a href="{{ asset('backend/image/teams') . '/' . $team->image }}" class="grayscale-link" data-lightbox="image"><img src="{{ asset('backend/image/teams') . '/' . $team->image }}" alt="" class="img-responsive"></a>
								<h5 class="mmb-title">{{ $team->name }}</h5>
								<h6 class="mmb-position">{{ $team->position }}</h6>
								<p class="mmb-desc">Donec tempus imperdiet venenatis for aliquet convallis. Donec lorem ipsum dolor nec loremis elitsit amet.</p>
							</div>
						</li>
						@endforeach
					</ul>
				</div>
				<!-- // team carousel -->
			</div>
			<!--/ col-sm-12 -->
		</div>
		<!--/ row -->
	</div>
	<!--/ container  -->
</section>

@endsection