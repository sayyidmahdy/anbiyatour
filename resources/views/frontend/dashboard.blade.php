

@extends('template.frontend.index')
@section('content')
<!-- Slideshow - iOS Slider alternative element with animateme scroll efect, custom height and bottom mask style 3 -->
<div class="kl-slideshow iosslider-slideshow uh_light_gray maskcontainer--mask3 iosslider--custom-height scrollme">
	<!-- Loader -->
	<div class="kl-loader">
		<svg version="1.1" id="loader-1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="40px" height="40px" viewbox="0 0 40 40" enable-background="new 0 0 40 40" xml:space="preserve">
			<path opacity="0.2" fill="#000" d="M20.201,5.169c-8.254,0-14.946,6.692-14.946,14.946c0,8.255,6.692,14.946,14.946,14.946 s14.946-6.691,14.946-14.946C35.146,11.861,28.455,5.169,20.201,5.169z M20.201,31.749c-6.425,0-11.634-5.208-11.634-11.634 c0-6.425,5.209-11.634,11.634-11.634c6.425,0,11.633,5.209,11.633,11.634C31.834,26.541,26.626,31.749,20.201,31.749z"></path>
			<path fill="#000" d="M26.013,10.047l1.654-2.866c-2.198-1.272-4.743-2.012-7.466-2.012h0v3.312h0 C22.32,8.481,24.301,9.057,26.013,10.047z" transform="rotate(98.3774 20 20)">
				<animatetransform attributetype="xml" attributename="transform" type="rotate" from="0 20 20" to="360 20 20" dur="0.5s" repeatcount="indefinite"></animatetransform>
			</path>
		</svg>
	</div>
	<!-- Loader -->

	<div class="bgback">
	</div>

	<!-- Animated Sparkles -->
	<div class="th-sparkles"></div>
	<!--/ Animated Sparkles -->

	<!-- iOS Slider wrapper with animateme scroll efect -->
	<div class="iosSlider kl-slideshow-inner animateme" data-trans="6000" data-autoplay="1" data-infinite="true" data-when="span" data-from="0" data-to="0.75" data-translatey="300" data-easing="linear">
		<!-- Slides -->
		<div class="kl-iosslider hideControls">
			@foreach($slideshow as $row)
			<!-- Slide 2 -->
			<div class="item iosslider__item">
				<!-- Image -->
				<div class="slide-item-bg" style="background-image:url({{ asset('backend/image/slideshow') . '/' . $row->image }});">
				</div>
				<!--/ Image -->
			</div>
			<!--/ Slide 2 -->
			@endforeach
		</div>
		<!--/ Slides -->

		<!-- Navigation Controls - Prev -->
		<div class="kl-iosslider-prev">
			<!-- Arrow -->
			<span class="thin-arrows ta__prev"></span>

			<!-- Label -->
			<div class="btn-label">
				PREV
			</div>
		</div>
		<!-- Navigation Controls - Prev -->

		<!-- Navigation Controls - Next -->
		<div class="kl-iosslider-next">
			<!-- Arrow -->
			<span class="thin-arrows ta__next"></span>

			<!-- Label -->
			<div class="btn-label">
				NEXT
			</div>
		</div>
		<!--/ Navigation Controls - Next -->
	</div>
	<!--/ iOS Slider wrapper with animateme scroll efect -->

	<!-- Bullets -->
	<div class="kl-ios-selectors-block bullets2">
		<div class="selectors">
			@foreach($slideshow as $row)
			<div class="item iosslider__bull-item first">
			</div>
			@endforeach
		</div>
	</div>
	<!--/ Bullets -->

	<div class="scrollbarContainer">
	</div>

	<!-- Bottom mask style 3 -->
	<div class="kl-bottommask kl-bottommask--mask3">
		<svg width="5000px" height="57px" class="svgmask " viewBox="0 0 5000 57" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
			<defs>
				<filter x="-50%" y="-50%" width="200%" height="200%" filterUnits="objectBoundingBox" id="filter-mask3">
					<feOffset dx="0" dy="3" in="SourceAlpha" result="shadowOffsetInner1"></feOffset>
					<feGaussianBlur stdDeviation="2" in="shadowOffsetInner1" result="shadowBlurInner1"></feGaussianBlur>
					<feComposite in="shadowBlurInner1" in2="SourceAlpha" operator="arithmetic" k2="-1" k3="1" result="shadowInnerInner1"></feComposite>
					<feColorMatrix values="0 0 0 0 0   0 0 0 0 0   0 0 0 0 0  0 0 0 0.4 0" in="shadowInnerInner1" type="matrix" result="shadowMatrixInner1"></feColorMatrix>
					<feMerge>
						<feMergeNode in="SourceGraphic"></feMergeNode>
						<feMergeNode in="shadowMatrixInner1"></feMergeNode>
					</feMerge>
				</filter>
			</defs>
			<path d="M9.09383679e-13,57.0005249 L9.09383679e-13,34.0075249 L2418,34.0075249 L2434,34.0075249 C2434,34.0075249 2441.89,33.2585249 2448,31.0245249 C2454.11,28.7905249 2479,11.0005249 2479,11.0005249 L2492,2.00052487 C2492,2.00052487 2495.121,-0.0374751261 2500,0.000524873861 C2505.267,-0.0294751261 2508,2.00052487 2508,2.00052487 L2521,11.0005249 C2521,11.0005249 2545.89,28.7905249 2552,31.0245249 C2558.11,33.2585249 2566,34.0075249 2566,34.0075249 L2582,34.0075249 L5000,34.0075249 L5000,57.0005249 L2500,57.0005249 L1148,57.0005249 L9.09383679e-13,57.0005249 Z" class="bmask-bgfill" filter="url(#filter-mask3)" fill="#f7ecdc"></path>
		</svg>
		<i class="glyphicon glyphicon-chevron-down"></i>
    </div>
    <!--/ Bottom mask style 3 -->
</div>
<!--/ Slideshow - iOS Slider alternative element with animateme scroll efect, custom height and bottom mask style 3 -->

<!-- Content section -->


<section class="hg_section bg-peach ptop-80 pbottom-80">
	<div class="container">
		<div class="row">
			<!-- 
			<div class="col-md-12">
				<div class="kl-title-block clearfix text-center tbk-symbol--icon tbk--colored tbk-icon-pos--after-title pbottom-15">
					<h3 class="tbk__title montserrat fs-26 tcolor">LATEST COLLECTIONS</h3>

					<span class="tbk__symbol ">
						<span class="tbk__icon fs-28 light-gray2 glyphicon glyphicon-option-horizontal"></span>
					</span>
				</div>
			</div>

			@foreach($latest_product as $row)
			<div class="col-md-4 col-sm-4">
				<div class="box image-boxes imgboxes_style4 kl-title_style_left">
					<a href="{{ url('product/detail/' . $row->slug) }}" class="imgboxes4_link imgboxes-wrapper" title="{{ $row->title }}">
						<img src="{{ asset('backend/image/product') . '/' . $row->image }}" class="img-responsive imgbox_image cover-fit-img" width="640" height="425" alt="{{ $row->title }}" title="{{ $row->title }}" />

						<span class="imgboxes-border-helper"></span>

						<h3 class="m_title imgboxes-title">{{ $row->title }} <span class="imgbox-title">{{ $row->currency .number_format($row->price, 0) }}</span></h3>
					</a>

					<p>
						{!! substr($row->description, 0, 158) !!}...
					</p>
				</div>
			</div>
			@endforeach
			
			-->

			@foreach($type as $type)
				<div class="col-md-12">
					<div class="kl-title-block clearfix text-center tbk-symbol--icon tbk--colored tbk-icon-pos--after-title pbottom-15">
						<h3 class="tbk__title montserrat fs-26 tcolor">{{ $type->name }}</h3>

						<span class="tbk__symbol ">
							<span class="tbk__icon fs-28 light-gray2 glyphicon glyphicon-option-horizontal"></span>
						</span>
					</div>
				</div>
				@foreach($product as $row)
					@if($row->type == $type->id)
					<div class="col-md-4 col-sm-4">
						<div class="box image-boxes imgboxes_style4 kl-title_style_left">
							<a href="{{ url('product/detail/' . $row->slug) }}" class="imgboxes4_link imgboxes-wrapper" title="{{ $row->title }}">
								<img src="{{ asset('backend/image/product') . '/' . $row->image }}" class="img-responsive imgbox_image cover-fit-img" width="640" height="425" alt="{{ $row->title }}" title="{{ $row->title }}" />

								<span class="imgboxes-border-helper"></span>

								<h3 class="m_title imgboxes-title">{{ $row->title }} <span class="imgbox-title">{{ $row->currency . number_format($row->price, 0) }}</span></h3>
							</a>

							<p>
								{!! substr($row->description, 0, 158) !!}...
							</p>
						</div>
					</div>
					@endif
				@endforeach
			@endforeach
		</div>
	</div>
</section>

<section class="hg_section bg-peach pt-80 pb-80">
	<div class="container">
		<div class="row">
			<div class="col-md-12 col-sm-12">
				<!-- Title element custom -->
				<div class="kl-title-block clearfix text-center tbk-icon-pos--after-title pbottom-50">
					<!-- Title with custom font size, line height  and thin style -->
					<h3 class="tbk__title fs-32 lh-52 fw-thin">THE TEAM</h3>
					<!--/ Title with custom font size, line height and thin style -->

					<!-- Title bottom symbol -->
					<div class="symbol-line">
						<span class="kl-icon tcolor icon-spinner10"></span>
					</div>
					<!--/ Title bottom symbol -->

					<!-- Sub-title with custom font size -->
					<h4 class="tbk__subtitle fs-14">APPROPRIATELY MORPH TECHNICALLY SOUND LEADERSHIP SKILLS</h4>
					<!--/ Sub-title with custom font size -->
				</div>
				<!--/ Title element custom -->
			</div>
			<!--/ col-md-12 col-sm-12 -->

			<div class="col-sm-12">
				<div class="team-carousel caroufredsel stg-slim-arrows" data-setup='{ "navigation": true, "pagination": false, "width": "variable", "items_width": "360", "items_height": "variable", "auto_duration": 9000, "items": {"min":1, "max":4} }'>
					<ul class="slides">
						@foreach($teams as $team)
						<li>
							<div class="team-member tm-hover text-center">
								<a href="{{ asset('backend/image/teams') . '/' . $team->image }}" class="grayscale-link" data-lightbox="image"><img src="{{ asset('backend/image/teams') . '/' . $team->image }}" alt="" class="img-responsive"></a>
								<h5 class="mmb-title">{{ $team->name }}</h5>
								<h6 class="mmb-position">{{ $team->position }}</h6>
								<p class="mmb-desc">Donec tempus imperdiet venenatis for aliquet convallis. Donec lorem ipsum dolor nec loremis elitsit amet.</p>
							</div>
						</li>
						@endforeach
					</ul>
				</div>
				<!-- // team carousel -->
			</div>
			<!--/ col-sm-12 -->
		</div>
		<!--/ row -->
	</div>
	<!--/ container  -->
</section>


<section class="hg_section hg_section--relative ptop-65 pbottom-80" style="background-color: #c2a73e;">
	<!-- Background -->
	<div class="kl-bg-source">
		<!-- Gloss overlay -->
		<div class="kl-bg-source__overlay-gloss"></div>
		<!--/ Gloss overlay -->
	</div>
	<!--/ Background -->

	<!-- Content -->
	<div class="container">
		<div class="row">
			<div class="col-md-12 col-sm-12">
				<!-- Testimonials & Partners element - light text style -->
				<div class="testimonials-partners testimonials-partners--light">
					<!-- Testimonials  element-->
					<div class="ts-pt-testimonials clearfix">
						@php $no = 1; @endphp
						@foreach($testimonials as $row)
							@php $number = $no++; @endphp
							@if($number == 1)
							<div class="ts-pt-testimonials__item ts-pt-testimonials__item--size-2 ts-pt-testimonials__item--normal" style="margin-top:20px; ">
								<div class="ts-pt-testimonials__text">
									{{ $row->description }}
								</div>
								<div class="ts-pt-testimonials__infos ts-pt-testimonials__infos--">
									<div class="ts-pt-testimonials__img" style="background-image:url('{{ asset('backend/image/testimonials') . '/' . $row->image }}');" title="JIMMY FERRARA">
									</div>
									<h4 class="ts-pt-testimonials__name">{{ $row->user }}</h4>
									<div class="ts-pt-testimonials__position">
										{{ $row->company }}
									</div>
									<div class="ts-pt-testimonials__stars ts-pt-testimonials__stars--5">
										<span class="glyphicon glyphicon-star"></span>
										<span class="glyphicon glyphicon-star"></span>
										<span class="glyphicon glyphicon-star"></span>
										<span class="glyphicon glyphicon-star"></span>
										<span class="glyphicon glyphicon-star"></span>
									</div>
								</div>
							</div>
							@elseif($number == 2)
							<div class="ts-pt-testimonials__item ts-pt-testimonials__item--size-1 ts-pt-testimonials__item--normal" style=" ">
								<div class="ts-pt-testimonials__text">
									{{ $row->description }}
								</div>

								<div class="ts-pt-testimonials__infos ts-pt-testimonials__infos--">
									<div class="ts-pt-testimonials__img" style="background-image:url('{{ asset('backend/image/testimonials') . '/' . $row->image }}');" title="PERRY ANDREWS">
									</div>

									<h4 class="ts-pt-testimonials__name">{{ $row->user }}</h4>

									<div class="ts-pt-testimonials__position">
										{{ $row->company }}
									</div>

									<div class="ts-pt-testimonials__stars ts-pt-testimonials__stars--4">
										<span class="glyphicon glyphicon-star"></span>
										<span class="glyphicon glyphicon-star"></span>
										<span class="glyphicon glyphicon-star"></span>
										<span class="glyphicon glyphicon-star"></span>
										<span class="glyphicon glyphicon-star"></span>
									</div>
								</div>
							</div>
							@else
							<div class="ts-pt-testimonials__item ts-pt-testimonials__item--size-1 ts-pt-testimonials__item--reversed" style=" ">
								<div class="ts-pt-testimonials__infos ts-pt-testimonials__infos--">
									<div class="ts-pt-testimonials__img" style="background-image:url('{{ asset('backend/image/testimonials') . '/' . $row->image }}');" title="SAMMY BROWNS">
									</div>

									<h4 class="ts-pt-testimonials__name">{{ $row->user }}</h4>

									<div class="ts-pt-testimonials__position">
										{{ $row->company }}
									</div>

									<div class="ts-pt-testimonials__stars ts-pt-testimonials__stars--5">
										<span class="glyphicon glyphicon-star"></span>
										<span class="glyphicon glyphicon-star"></span>
										<span class="glyphicon glyphicon-star"></span>
										<span class="glyphicon glyphicon-star"></span>
										<span class="glyphicon glyphicon-star"></span>
									</div>
								</div>

								<div class="ts-pt-testimonials__text">
									{{ $row->description }}
								</div>
							</div>
							@endif
						@endforeach
					</div>
					<!--/ Testimonials element - .ts-pt-testimonials-->

					<!-- Separator for testimonials-partners elements -->
					<div class="testimonials-partners__separator clearfix">
					</div>
					<!--/ Separator for testimonials-partners elements -->

					<!-- Partners element -->
					<div class="ts-pt-partners ts-pt-partners--y-title clearfix">
						<!-- Title -->
						<div class="ts-pt-partners__title">
							PARTNER
						</div>
						<!--/ Title -->

						<!-- Partners carousel wrapper -->
						<div class="ts-pt-partners__carousel-wrapper">
							<div class="ts-pt-partners__carousel">
								<!-- Item #1 -->
								<div class="ts-pt-partners__carousel-item">
									<!-- Partner image link -->
									<a class="ts-pt-partners__link" href="#" target="_self" title="">
										<!-- Image -->
										<img class="ts-pt-partners__img img-responsive" src="https://demo.kallyas.net/wp-content/uploads/2015/08/logo1.svg" alt="" title="logo1.svg">
										<!--/ Image -->
									</a>
									<!--/ Partner image link -->
								</div>
								<!--/ Item #1 -->
								@foreach($flight as $row_flight)
								<!-- Item #1 -->
								<div class="ts-pt-partners__carousel-item">
									<!-- Partner image link -->
									<a class="ts-pt-partners__link" href="#" target="_self" title="">
										<!-- Image -->
										<img class="ts-pt-partners__img" src="{{ asset('backend/image/flight') . '/' . $row_flight->image }}" alt="" title="" />
										<!--/ Image -->
									</a>
									<!--/ Partner image link -->
								</div>
								<!--/ Item #1 -->
								@endforeach
								<!-- Item #2 -->
								<div class="ts-pt-partners__carousel-item">
									<!-- Partner image link -->
									<a class="ts-pt-partners__link" href="#" target="_self" title="">
										<!-- Image -->
										<img class="ts-pt-partners__img" src="{{ asset('assets/frontend/images/logo7.svg') }}" alt="" title="" />
										<!--/ Image -->
									</a>
									<!--/ Partner image link -->
								</div>
								<!--/ Item #2 -->

								<!-- Item #3 -->
								<div class="ts-pt-partners__carousel-item">
									<!-- Partner image link -->
									<a class="ts-pt-partners__link" href="#" target="_self" title="">
										<!-- Image -->
										<img class="ts-pt-partners__img" src="{{ asset('assets/frontend/images/logo8.svg') }}" alt="" title="" />
										<!--/ Image -->
									</a>
									<!--/ Partner image link -->
								</div>
								<!--/ Item #3 -->

								<!-- Item #4 -->
								<div class="ts-pt-partners__carousel-item">
									<!-- Partner image link -->
									<a class="ts-pt-partners__link" href="#" target="_self" title="">
										<!-- Image -->
										<img class="ts-pt-partners__img" src="{{ asset('assets/frontend/images/logo1.svg') }}" alt="" title="" />
										<!--/ Image -->
									</a>
									<!--/ Partner image link -->
								</div>
								<!--/ Item #4 -->

								<!-- Item #5 -->
								<div class="ts-pt-partners__carousel-item">
									<!-- Partner image link -->
									<a class="ts-pt-partners__link" href="#" target="_self" title="">
										<!-- Image -->
										<img class="ts-pt-partners__img" src="{{ asset('assets/frontend/images/logo2.svg') }}" alt="" title="" />
										<!--/ Image -->
									</a>
									<!--/ Partner image link -->
								</div>
								<!--/ Item #5 -->

								<!-- Item #6 -->
								<div class="ts-pt-partners__carousel-item">
									<!-- Partner image link -->
									<a class="ts-pt-partners__link" href="#" target="_self" title="">
										<!-- Image -->
										<img class="ts-pt-partners__img" src="{{ asset('assets/frontend/images/logo3.svg') }}" alt="" title="" />
										<!--/ Image -->
									</a>
									<!--/ Partner image link -->
								</div>
								<!--/ Item #6 -->

								<!-- Item #7 -->
								<div class="ts-pt-partners__carousel-item">
									<!-- Partner image link -->
									<a class="ts-pt-partners__link" href="#" target="_self" title="">
										<!-- Image -->
										<img class="ts-pt-partners__img" src="{{ asset('assets/frontend/images/logo4.svg') }}" alt="" title="" />
										<!--/ Image -->
									</a>
									<!--/ Partner image link -->
								</div>
								<!--/ Item #7 -->

								<!-- Item  #8-->
								<div class="ts-pt-partners__carousel-item">
									<!-- Partner image link -->
									<a class="ts-pt-partners__link" href="#" target="_self" title="">
										<!-- Image -->
										<img class="ts-pt-partners__img" src="{{ asset('assets/frontend/images/logo5.svg') }}" alt="" title="" />
										<!--/ Image -->
									</a>
									<!--/ Partner image link -->
								</div>
								<!--/ Item #8 -->
							</div>
							<!--/ .ts-pt-partners__carousel -->
						</div>
						<!--/ Partners carousel wrapper -->
					</div>
					<!--/ Partners element - .ts-pt-partners -->
				</div>
				<!--/ Testimonials & Partners element - light text style -->
			</div>
			<!--/ col-md-12 col-sm-12 -->
		</div>
		<!--/ row -->
	</div>
	<!--/ Content .container -->
</section>

<section class="hg_section bg-white ptop-80 pbottom-80">
	<div class="full_width">
		<div class="row">
			<div class="col-md-12 col-sm-12">
				<!-- Recent Work carousel 1 style 2 element -->
				<div class="recentwork_carousel recentwork_carousel--2 clearfix">
					<div class="row">
						<div class="col-sm-4">
							<!-- Left side -->
							<div class="recentwork_carousel__left">
								<!-- Title -->
								<h3 class="recentwork_carousel__title m_title">News</h3>
								<!--/ Title -->

								<!-- Description -->
								<p class="recentwork_carousel__desc">
									Start your search with a look at the best rates on our site.
								</p>
								<!--/ Description -->

								<!-- View all button -->
								<a href="{{ url('news') }}" class="btn btn-fullcolor">VIEW ALL</a>
								<!--/ View all button -->

								<!-- Navigation controls -->
								<div class="controls recentwork_carousel__controls">
									<a href="#" class="prev recentwork_carousel__prev"><span class="glyphicon glyphicon-chevron-left"></span></a>
									<a href="#" class="next recentwork_carousel__next"><span class="glyphicon glyphicon-chevron-right"></span></a>
								</div>
								<!--/ Navigation controls - .recentwork_carousel__controls -->
							</div>
							<!--/ Left side - .recentwork_carousel__left -->
						</div>
						<!--/ col-sm-4 -->

						<div class="col-sm-8">
							<!-- Right side - carousel wrapper -->
							<div class="recentwork_carousel__crsl-wrapper">
								<ul class="recent_works1 fixclear recentwork_carousel__crsl">
									@foreach($news as $row)
									<!-- Item #1 -->
									<li>
										<!-- Portfolio link container -->
										<a href="{{ url('news/detail/' . $row->slug) }}" class="recentwork_carousel__link" title="ROME">
											<!-- Hover container -->
											<div class="hover recentwork_carousel__hover">
												<!-- Background image with custom height -->
												<div style="height: 240px; background-image:url('{{ asset('backend/image/news') . '/' . $row->image }}');" class="recentwork_carousel__img">
												</div>
												<!--/ Background image with custom height -->

												<!-- Hover shadow overlay -->
												<span class="hov recentwork_carousel__hov"></span>
												<!--/ Hover shadow overlay -->
											</div>
											<!--/ Hover container -->

											<!-- Content details -->
											<div class="details recentwork_carousel__details">
												<!-- Tags/Category -->
												<span class="recentwork_carousel__cat">Save up to 30%</span>
												<!--/ Tags/Category -->

												<!-- Title -->
												<h4 class="recentwork_carousel__crsl-title">ROME</h4>
												<!--/ Title -->
											</div>
											<!--/ Content details -->
										</a>
										<!--/ Portfolio link container -->
									</li>
									<!--/ Item #1 -->
									@endforeach
								</ul>
							</div>
							<!--/  Right side - carousel wrapper - .recentwork_carousel__crsl-wrapper -->
						</div>
						<!--/ col-sm-8 -->
					</div>
					<!--/ row -->
				</div>
				<!--/ Recent Work carousel 1 style 2 element -->
			</div>
			<!--/ col-md-12 col-sm-12 -->
		</div>
		<!--/ row -->
    </div>
    <!--/ .full_width -->
</section>
@endsection