@extends('template.frontend.index')
@section('content')
<!-- Page sub-header + Bottom mask style 3 -->
		<div id="page_header" class="page-subheader site-subheader-cst uh_flat_dark_blue maskcontainer--mask3">
			<div class="bgback">
			</div>

			<!-- Animated Sparkles -->
			<div class="th-sparkles"></div>
			<!--/ Animated Sparkles -->

			<!-- Background source -->
			<div class="kl-bg-source">
				<!-- Background image -->
				<div class="kl-bg-source__bgimage" style="background-image:url({{ asset('assets/frontend/images/_niches/travel/plane.jpg')}}); background-repeat:no-repeat; background-attachment:scroll; background-position-x:center; background-position-y:center; background-size:cover">
				</div>
				<!--/ Background image -->

				<!-- Gradient overlay -->
				<div class="kl-bg-source__overlay" style="background:rgba(0,94,176,0.8); background: -moz-linear-gradient(left, rgba(0,94,176,0.8) 0%, rgba(0,202,255,0.9) 100%); background: -webkit-gradient(linear, left top, right top, color-stop(0%,rgba(0,94,176,0.8)), color-stop(100%,rgba(0,202,255,0.9))); background: -webkit-linear-gradient(left, rgba(0,94,176,0.8) 0%,rgba(0,202,255,0.9) 100%); background: -o-linear-gradient(left, rgba(0,94,176,0.8) 0%,rgba(0,202,255,0.9) 100%); background: -ms-linear-gradient(left, rgba(0,94,176,0.8) 0%,rgba(0,202,255,0.9) 100%); background: linear-gradient(to right, rgba(0,94,176,0.8) 0%,rgba(0,202,255,0.9) 100%); ">
				</div>
				<!--/ Gradient overlay -->
			</div>
			<!--/ Background source -->

			<!-- Sub-Header content wrapper -->
			<div class="ph-content-wrap">
				<div class="ph-content-v-center">
					<div class="container">
						<div class="row">
							<div class="col-sm-6">
								<!-- Breadcrumbs -->
								<ul class="breadcrumbs fixclear">
									<li><a href="{{ url('/') }}">Home</a></li>
									<li><a href="{{ url('/product/' . $product->slug) }}">{{ $product->slug }}</a></li>
								</ul>
								<!--/ Breadcrumbs -->

								<div class="clearfix"></div>
							</div>
							<!--/ col-sm-6 -->

							<div class="col-sm-6">
								<!-- Sub-header titles -->
								<div class="subheader-titles">
									<h2 class="subheader-maintitle">{{ $product->title }}</h2>
									<h4 class="subheader-subtitle">☆☆☆☆☆</h4>
								</div>
								<!--/ Sub-header titles -->
							</div>
							<!--/ col-sm-6 -->
						</div>
						<!--/ row -->
					</div>
					<!--/ container -->
				</div>
				<!--/ .ph-content-v-center -->
			</div>
			<!--/ Sub-Header content wrapper -->

			<!-- Bottom mask style 3 -->
			<div class="kl-bottommask kl-bottommask--mask3">
				<svg width="5000px" height="57px" class="svgmask " viewBox="0 0 5000 57" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
					<defs>
						<filter x="-50%" y="-50%" width="200%" height="200%" filterUnits="objectBoundingBox" id="filter-mask3">
							<feOffset dx="0" dy="3" in="SourceAlpha" result="shadowOffsetInner1"></feOffset>
							<feGaussianBlur stdDeviation="2" in="shadowOffsetInner1" result="shadowBlurInner1"></feGaussianBlur>
							<feComposite in="shadowBlurInner1" in2="SourceAlpha" operator="arithmetic" k2="-1" k3="1" result="shadowInnerInner1"></feComposite>
							<feColorMatrix values="0 0 0 0 0   0 0 0 0 0   0 0 0 0 0  0 0 0 0.4 0" in="shadowInnerInner1" type="matrix" result="shadowMatrixInner1"></feColorMatrix>
							<feMerge>
								<feMergeNode in="SourceGraphic"></feMergeNode>
								<feMergeNode in="shadowMatrixInner1"></feMergeNode>
							</feMerge>
						</filter>
					</defs>
					<path d="M9.09383679e-13,57.0005249 L9.09383679e-13,34.0075249 L2418,34.0075249 L2434,34.0075249 C2434,34.0075249 2441.89,33.2585249 2448,31.0245249 C2454.11,28.7905249 2479,11.0005249 2479,11.0005249 L2492,2.00052487 C2492,2.00052487 2495.121,-0.0374751261 2500,0.000524873861 C2505.267,-0.0294751261 2508,2.00052487 2508,2.00052487 L2521,11.0005249 C2521,11.0005249 2545.89,28.7905249 2552,31.0245249 C2558.11,33.2585249 2566,34.0075249 2566,34.0075249 L2582,34.0075249 L5000,34.0075249 L5000,57.0005249 L2500,57.0005249 L1148,57.0005249 L9.09383679e-13,57.0005249 Z" class="bmask-bgfill" filter="url(#filter-mask3)" fill="#f5f5f5"></path>
				</svg>
			    <i class="glyphicon glyphicon-chevron-down"></i>
			</div>
			<!--/ Bottom mask style 3 -->
		</div>
		<!--/ Page sub-header + Bottom mask style 3 -->

		<!-- Hotel rooms carousel (Portfolio carousel) element section with custom paddings -->
		<section class="hg_section ptop-65 pbottom-0">
			<div class="container">
				<div class="row">
					<div class="col-md-12 col-sm-12">
						<!-- Portfolio carousel item -->
						<div class="portfolio-item">
							<div class="row">
								<div class="col-sm-6">
									<div class="ptcontent">
										<!-- Title -->
										<h3 class="title pt-content-title">
											<a href="portfolio-item.html">
												<span class="name">{{ $product->title }}</span>
											</a>
										</h3>
										<!--/ Title -->

										<!-- Item description -->
										<!-- <div class="pt-cat-desc">
											<p>
												{!! $product->description !!}
											</p>
										</div> -->
										<!--/ Item description -->

										<!-- Item details -->
										<ul class="portfolio-item-details travel clearfix">
											<li class="clearfix">Free internet</li>
											<li class="clearfix">SPA</li>
											<li class="clearfix">Air conditioning</li>
											<li class="clearfix">Bathroom telephone</li>
											<li class="clearfix">Rent a car</li>
										</ul>
										<!--/ Item details -->

										<!-- Link Buttons -->
										<!-- <div class="pt-itemlinks itemLinks">
											<a href="portfolio-item.html" class="btn btn-fullcolor" target="_self" title="SEE MORE" >SEE MORE </a>
											<a href="#" class="btn btn-lined lined-dark " target="_blank" title="LIVE PREVIEW">LIVE PREVIEW</a>
										</div> -->
										<!--/ Link Buttons -->
									</div>
									<!-- end item content -->
								</div>
								<!--/ col-sm-6 -->

								<div class="col-sm-6">
									<div class="ptcarousel ptcarousel--frames-modern">
										<div class="zn_simple_slider_container">
											<!-- Navigation controls -->
											<div class="th-controls controls">
												<a href="#" class="prev cfs--prev">
													<span class="glyphicon glyphicon-chevron-left icon-white"></span>
												</a>
												<a href="#" class="next cfs--next">
													<span class="glyphicon glyphicon-chevron-right icon-white"></span>
												</a>
											</div>
											<!--/ Navigation controls -->

											<!-- Carousel -->
											<ul class="zn_general_carousel cfs--default">
												<!-- Slide 1 -->
												<li class="item kl-has-overlay cfs--item">
													<!-- Intro image -->
													<div class="img-intro">
														<!-- Pop-up image -->
														<a href="{{ asset('backend/image/product') . '/' . $product->image }}" data-type="image" data-lightbox="image" title="GRAND HOTEL"></a>
														<!--/ Pop-up image -->

														<!-- Image -->
														<img src="{{ asset('backend/image/product') . '/' . $product->image }}" class="img-responsive" alt="GRAND HOTEL" title="GRAND HOTEL" />
														<!--/ Image -->

														<!-- Overlay -->
														<div class="overlay">
															<div class="overlay-inner">
																<span class="glyphicon glyphicon-picture"></span>
															</div>
														</div>
														<!--/ Overlay -->
													</div>
													<!--/ Intro image -->
												</li>
												<!--/ Slide 1 -->
											</ul>
										</div>
										<!--/ zn_simple_slider_container -->
									</div>
									<!-- end ptcarousel -->
								</div>
								<!--/ col-sm-6 -->
							</div>
							<!--/ row -->
						</div>
						<!--/ Portfolio carousel item -->

						<!-- Separator -->
						<div class="hg_separator clearfix">
						</div>
					</div>
					<!--/ col-md-12 col-sm-12 -->
				</div>
				<!--/ row -->
			</div>
			<!--/ container -->
		</section>
		<!--/ Hotel rooms carousel (Portfolio carousel) element section with custom paddings -->

		<section id="content" class="hg_section">
			<div class="container">
				<div class="row">
					<!-- Content with right sidebar -->
					<div class="right_sidebar col-md-12">
						<!-- Product -->
						<div class="product">
							<div class="tabbable">
								<!-- Navigation -->
								<ul class="nav fixclear">
									<li class="active"><a href="#tab-description" data-toggle="tab">Description</a></li>
									<!-- <li class=""><a href="#tab-reviews" data-toggle="tab">Reviews (3)</a></li> -->
								</ul>

								<!-- Tab content -->
								<div class="tab-content">
									<!-- Description -->
									<div class="tab-pane active" id="tab-description">
										<h2 class="fs-18">PRODUCT DESCRIPTION</h2>
										<p>
											{!! $product->description !!}
										</p>
									</div>
									<!--/ Description -->
								</div>
								<!--/ Tab content -->
							</div>
						</div>
						<!--/ Product -->
					</div>
					<!--/ Content with right sidebar -->		
				</div>
				<!--/ row -->
			</div>
			<!--/ container -->
		</section>
		
@endsection