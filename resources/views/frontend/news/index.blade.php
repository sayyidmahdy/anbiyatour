@extends('template.frontend.index')
@section('content')
<!-- Page sub-header + Bottom mask style 3 -->
<div id="page_header" class="page-subheader site-subheader-cst uh_flat_dark_blue maskcontainer--mask3">
	<div class="bgback">
	</div>

	<!-- Animated Sparkles -->
	<div class="th-sparkles"></div>
	<!--/ Animated Sparkles -->

	<!-- Background source -->
	<div class="kl-bg-source">
		<!-- Background image -->
		<div class="kl-bg-source__bgimage" style="background-image:url({{ asset('assets/frontend/images/_niches/travel/plane.jpg')}}); background-repeat:no-repeat; background-attachment:scroll; background-position-x:center; background-position-y:center; background-size:cover">
		</div>
		<!--/ Background image -->

		<!-- Gradient overlay -->
		<div class="kl-bg-source__overlay" style="background:rgba(0,94,176,0.8); background: -moz-linear-gradient(left, rgba(0,94,176,0.8) 0%, rgba(0,202,255,0.9) 100%); background: -webkit-gradient(linear, left top, right top, color-stop(0%,rgba(0,94,176,0.8)), color-stop(100%,rgba(0,202,255,0.9))); background: -webkit-linear-gradient(left, rgba(0,94,176,0.8) 0%,rgba(0,202,255,0.9) 100%); background: -o-linear-gradient(left, rgba(0,94,176,0.8) 0%,rgba(0,202,255,0.9) 100%); background: -ms-linear-gradient(left, rgba(0,94,176,0.8) 0%,rgba(0,202,255,0.9) 100%); background: linear-gradient(to right, rgba(0,94,176,0.8) 0%,rgba(0,202,255,0.9) 100%); ">
		</div>
		<!--/ Gradient overlay -->
	</div>
	<!--/ Background source -->

	<!-- Sub-Header content wrapper -->
	<div class="ph-content-wrap">
		<div class="ph-content-v-center">
			<div class="container">
				<div class="row">
					<div class="col-sm-6">
						<!-- Breadcrumbs -->
						<ul class="breadcrumbs fixclear">
							<li><a href="{{ url('/') }}">Home</a></li>
							<li><a href="{{ url('/news/') }}">News</a></li>
						</ul>
						<!--/ Breadcrumbs -->

						<div class="clearfix"></div>
					</div>
					
					<!--/ col-sm-6 -->
				</div>
				<!--/ row -->
			</div>
			<!--/ container -->
		</div>
		<!--/ .ph-content-v-center -->
	</div>
	<!--/ Sub-Header content wrapper -->

	<!-- Bottom mask style 3 -->
	<div class="kl-bottommask kl-bottommask--mask3">
		<svg width="5000px" height="57px" class="svgmask " viewBox="0 0 5000 57" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
			<defs>
				<filter x="-50%" y="-50%" width="200%" height="200%" filterUnits="objectBoundingBox" id="filter-mask3">
					<feOffset dx="0" dy="3" in="SourceAlpha" result="shadowOffsetInner1"></feOffset>
					<feGaussianBlur stdDeviation="2" in="shadowOffsetInner1" result="shadowBlurInner1"></feGaussianBlur>
					<feComposite in="shadowBlurInner1" in2="SourceAlpha" operator="arithmetic" k2="-1" k3="1" result="shadowInnerInner1"></feComposite>
					<feColorMatrix values="0 0 0 0 0   0 0 0 0 0   0 0 0 0 0  0 0 0 0.4 0" in="shadowInnerInner1" type="matrix" result="shadowMatrixInner1"></feColorMatrix>
					<feMerge>
						<feMergeNode in="SourceGraphic"></feMergeNode>
						<feMergeNode in="shadowMatrixInner1"></feMergeNode>
					</feMerge>
				</filter>
			</defs>
			<path d="M9.09383679e-13,57.0005249 L9.09383679e-13,34.0075249 L2418,34.0075249 L2434,34.0075249 C2434,34.0075249 2441.89,33.2585249 2448,31.0245249 C2454.11,28.7905249 2479,11.0005249 2479,11.0005249 L2492,2.00052487 C2492,2.00052487 2495.121,-0.0374751261 2500,0.000524873861 C2505.267,-0.0294751261 2508,2.00052487 2508,2.00052487 L2521,11.0005249 C2521,11.0005249 2545.89,28.7905249 2552,31.0245249 C2558.11,33.2585249 2566,34.0075249 2566,34.0075249 L2582,34.0075249 L5000,34.0075249 L5000,57.0005249 L2500,57.0005249 L1148,57.0005249 L9.09383679e-13,57.0005249 Z" class="bmask-bgfill" filter="url(#filter-mask3)" fill="#f5f5f5"></path>
		</svg>
	    <i class="glyphicon glyphicon-chevron-down"></i>
	</div>
	<!--/ Bottom mask style 3 -->
</div>
<!--/ Page sub-header + Bottom mask style 3 -->

<section class="hg_section ptop-50">          
	<div class="container">
		<div class="row">					
			<div class="col-md-12 col-sm-12">
				<div class="itemListView clearfix eBlog">
					<div class="itemList">
						<!-- <div class="itemContainer featured-post">
							<div class="zn_full_image">
								<img class="zn_post_thumbnail" src="images/blog1.jpg" alt="">
							</div>

							<div class="itemFeatContent">
								<div class="itemFeatContent-inner">
									<div class="itemHeader">
										<h3 class="itemTitle">
											<a href="blog-post.html" title="Progressively">Progressively repurpose cutting-edge models</a>
										</h3>

										<div class="post_details">
											<span class="catItemDateCreated">
											Friday, 07 August 2017 </span>
											<span class="catItemAuthor">by <a href="#" title="Laurentiu" rel="author">Laurentiu</a></span>
										</div>
									</div>

									<ul class="itemLinks clearfix">
										<li class="itemCategory">
										<span class="glyphicon glyphicon-folder-close"></span>
										<span>Published in</span>
										<a href="blog.html" title="">Mobile</a>, <a href="blog.html" title="">Networking</a></li>
									</ul>

									<div class="itemComments">
										<a href="blog.html">No Comments</a>
									</div>
									<div class="clearfix">
									</div>
								</div>
							</div>
						</div>
						<div class="clear"></div> -->
						<!--/ Blog item container -->

						@foreach($news as $row)
							<!-- Blog item container -->
							<div class="itemContainer">
								<!-- Post header -->
								<div class="itemHeader">
									<!-- Title -->
									<h3 class="itemTitle">
										<a href="blog-post.html" title="Enthusiastically">Enthusiastically administrate ubiquitous</a>
									</h3>
									<!--/ Title -->

									<!-- Post details -->
									<div class="post_details">
										<span class="catItemDateCreated">
											@php
												$date = date_create($row->created_at);
												$created_at = date_format($date,"l d-M-Y");
												echo $created_at;
											@endphp
										</span>
									</div>
									<!-- Post details -->
								</div>
								<!--/ Post header -->

								<!-- Post content body-->
								<div class="itemBody">
									<!-- Item intro text -->
									<div class="itemIntroText">
										<!-- Post image -->
										<div class="hg_post_image">
											<a href="blog-post.html" class="pull-left" title="Enthusiastically">
												<img src="{{ asset('backend/image/news') . '/' . $row->image }}" class="" width="457" height="320" alt="Enthusiastically" title="Enthusiastically" />
											</a>
										</div>
										<!--/ Post image -->
										<p>
											{{ substr($row->content, 100) }}
										</p>
									</div>
									<!-- end Item intro text -->
									<div class="clear"></div>

									<!-- Item tags -->
									<div class="itemBottom clearfix">
										<div class="itemTagsBlock">
											<a href="#" rel="tag" title="">Tour</a><a href="#" rel="tag" title="">Travel</a>
											<div class="clear">
											</div>
										</div>
										<!-- end tags blocks -->

										<!-- Read more button -->
										<div class="itemReadMore">
											<a class="btn btn-fullcolor readMore" href="{{ url('news/detail/' . $row->slug) }}" title="">Read more</a>
										</div>
										<!--/ Read more button -->
									</div>
									<!--/ Item tags -->
									<div class="clear"></div>
								</div>
								<!--/ Post content body-->

								<!-- Post category -->
								<ul class="itemLinks clearfix">
									<li class="itemCategory">
									<span class="glyphicon glyphicon-folder-close"></span>
									<span>Published in</span>
									<a href="blog.html" title="">Mobile</a></li>
								</ul>
								<!--/ Post category -->

								<!-- Post comments -->
								<div class="itemComments">
									<a href="blog.html" title="">No Comments</a>
								</div>
								<!--/ Post comments -->
								<div class="clear">
								</div>
							</div>
							<!--/ Blog item container -->
							<div class="clear"></div>
						@endforeach
					</div>
					<!--/ .itemList -->

					<!-- Pagination -->
					<!-- <ul class="pagination">
						<li class="pagination-prev">
							<span>
								<span></span>
								
							</span>
						</li>
						<li class="active">
							<span>1</span>
						</li>
						<li>
							<a href="#">
								<span>2</span>
							</a>
						</li>
						<li class="pagination-next">
							<a href="#">
								<span class="glyphicon glyphicon-menu-right"></span>
							</a>
						</li>
					</ul> -->
					<!--/ Pagination -->
				</div>
				<!--/ .itemListView -->
			</div>
			<!--/ col-md-9 col-sm-9 -->	
		</div>
		<!--/ row -->
	</div>
	<!--/ container -->
</section>

@endsection