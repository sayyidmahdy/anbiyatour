@extends('template.frontend.index')
@section('content')
<!-- Page sub-header + Bottom mask style 3 -->
<div id="page_header" class="page-subheader site-subheader-cst uh_flat_dark_blue maskcontainer--mask3">
	<div class="bgback">
	</div>

	<!-- Animated Sparkles -->
	<div class="th-sparkles"></div>
	<!--/ Animated Sparkles -->

	<!-- Background source -->
	<div class="kl-bg-source">
		<!-- Background image -->
		<div class="kl-bg-source__bgimage" style="background-image:url({{ asset('assets/frontend/images/_niches/travel/plane.jpg')}}); background-repeat:no-repeat; background-attachment:scroll; background-position-x:center; background-position-y:center; background-size:cover">
		</div>
		<!--/ Background image -->

		<!-- Gradient overlay -->
		<div class="kl-bg-source__overlay" style="background:rgba(0,94,176,0.8); background: -moz-linear-gradient(left, rgba(0,94,176,0.8) 0%, rgba(0,202,255,0.9) 100%); background: -webkit-gradient(linear, left top, right top, color-stop(0%,rgba(0,94,176,0.8)), color-stop(100%,rgba(0,202,255,0.9))); background: -webkit-linear-gradient(left, rgba(0,94,176,0.8) 0%,rgba(0,202,255,0.9) 100%); background: -o-linear-gradient(left, rgba(0,94,176,0.8) 0%,rgba(0,202,255,0.9) 100%); background: -ms-linear-gradient(left, rgba(0,94,176,0.8) 0%,rgba(0,202,255,0.9) 100%); background: linear-gradient(to right, rgba(0,94,176,0.8) 0%,rgba(0,202,255,0.9) 100%); ">
		</div>
		<!--/ Gradient overlay -->
	</div>
	<!--/ Background source -->

	<!-- Sub-Header content wrapper -->
	<div class="ph-content-wrap">
		<div class="ph-content-v-center">
			<div class="container">
				<div class="row">
					<div class="col-sm-6">
						<!-- Breadcrumbs -->
						<ul class="breadcrumbs fixclear">
							<li><a href="{{ url('/') }}">Home</a></li>
							<li><a href="{{ url('/news/') }}">News</a></li>
						</ul>
						<!--/ Breadcrumbs -->

						<div class="clearfix"></div>
					</div>
					<!--/ col-sm-6 -->

					<div class="col-sm-6">
						<!-- Sub-header titles -->
						<div class="subheader-titles">
							<h2 class="subheader-maintitle">{{ $news->title }}</h2>
							<h4 class="subheader-subtitle">☆☆☆☆☆</h4>
						</div>
						<!--/ Sub-header titles -->
					</div>
					<!--/ col-sm-6 -->
				</div>
				<!--/ row -->
			</div>
			<!--/ container -->
		</div>
		<!--/ .ph-content-v-center -->
	</div>
	<!--/ Sub-Header content wrapper -->

	<!-- Bottom mask style 3 -->
	<div class="kl-bottommask kl-bottommask--mask3">
		<svg width="5000px" height="57px" class="svgmask " viewBox="0 0 5000 57" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
			<defs>
				<filter x="-50%" y="-50%" width="200%" height="200%" filterUnits="objectBoundingBox" id="filter-mask3">
					<feOffset dx="0" dy="3" in="SourceAlpha" result="shadowOffsetInner1"></feOffset>
					<feGaussianBlur stdDeviation="2" in="shadowOffsetInner1" result="shadowBlurInner1"></feGaussianBlur>
					<feComposite in="shadowBlurInner1" in2="SourceAlpha" operator="arithmetic" k2="-1" k3="1" result="shadowInnerInner1"></feComposite>
					<feColorMatrix values="0 0 0 0 0   0 0 0 0 0   0 0 0 0 0  0 0 0 0.4 0" in="shadowInnerInner1" type="matrix" result="shadowMatrixInner1"></feColorMatrix>
					<feMerge>
						<feMergeNode in="SourceGraphic"></feMergeNode>
						<feMergeNode in="shadowMatrixInner1"></feMergeNode>
					</feMerge>
				</filter>
			</defs>
			<path d="M9.09383679e-13,57.0005249 L9.09383679e-13,34.0075249 L2418,34.0075249 L2434,34.0075249 C2434,34.0075249 2441.89,33.2585249 2448,31.0245249 C2454.11,28.7905249 2479,11.0005249 2479,11.0005249 L2492,2.00052487 C2492,2.00052487 2495.121,-0.0374751261 2500,0.000524873861 C2505.267,-0.0294751261 2508,2.00052487 2508,2.00052487 L2521,11.0005249 C2521,11.0005249 2545.89,28.7905249 2552,31.0245249 C2558.11,33.2585249 2566,34.0075249 2566,34.0075249 L2582,34.0075249 L5000,34.0075249 L5000,57.0005249 L2500,57.0005249 L1148,57.0005249 L9.09383679e-13,57.0005249 Z" class="bmask-bgfill" filter="url(#filter-mask3)" fill="#f5f5f5"></path>
		</svg>
	    <i class="glyphicon glyphicon-chevron-down"></i>
	</div>
	<!--/ Bottom mask style 3 -->
</div>
<!--/ Page sub-header + Bottom mask style 3 -->
<section class="hg_section ptop-50">          
	<div class="container">
		<div class="row">
			<!--/ col-md-3 col-sm-3 -->

			<div class="col-md-12 col-sm-12">
				<!-- Post content -->
				<div id="th-content-post">
					<!-- Post div wrapper -->
					<div>
						<!-- Post page title -->
						<h1 class="page-title black">{{ $news->title }}</h1>
						<!--/ Post page title -->

						<!-- Post layout -->
						<div class="itemView clearfix eBlog">
							<!-- Post header -->
							<div class="itemHeader">
								<!-- Post details -->
								<div class="post_details">

									<!-- Separator -->
									<span class="infSep"> / </span>
									<!--/ Separator -->

									<!-- Date created -->
									<span class="itemDateCreated"><span class="glyphicon glyphicon-calendar"></span> {{ $created_at }}</span>
									<!--/ Date created -->

									<!-- Separator -->
									<span class="infSep"> / </span>
									<!--/ Separator -->

									<!-- Comments block -->
									<span class="itemCommentsBlock"></span>
									<!--/ Comments block -->

									<!-- Category -->
									<span class="itemCategory">
										<span class="glyphicon glyphicon-folder-close"></span>
										Published in 
									</span>
									<a href="blog.html"><strong>Website</a>
									<!--/ Category -->
								</div>
								<!--/ Post details -->
							</div>
							<!--/ Post header -->

							<!-- Post body -->
							<div class="itemBody">
								<!-- Blog Image -->
								<a data-lightbox="image" href="{{ asset('backend/image/news') . '/' . $news->image }}" class="hoverBorder pull-left" style="margin-right: 20px;margin-bottom:4px;">
									<span class="hoverBorderWrapper">
										<img src="{{ asset('backend/image/news') . '/' . $news->image }}" alt="Post Image" title="ipad-605439_640" />
										<span class="theHoverBorder"></span>
									</span>
								</a>
								<!--/ Blog Image -->
								<p style="font-weight: 400">{{ $news->content }}</p>
								
								<!--/ Blog Content -->
							</div>
							<div class="itemTagsBlock">
										<span>Tagged under:</span>
										<a href="#" rel="tag">Tour</a>, <a href="#" rel="tag">Travel</a>
										<div class="clear"></div>
									</div>
							<!--/ Post body -->
							<div class="clear"></div>

							<!-- Related articles -->
							<div class="related-articles">
								<!-- Title -->
								<h3 class="rta-title">What you can read next</h3>
								<!--/ Title -->

								<div class="row">
									@foreach($latest_news as $row)
									<div class="col-sm-4">
										<!-- Related article post -->
										<div class="rta-post">
											<!-- Image -->
											<a href="blog.html">
												<img src="{{ asset('backend/image/news') . '/' . $row->image }}" class="img-responsive" width="370" height="240" alt="">
											</a>
											<!--/ Image -->

											<!-- Title -->
											<h5>
												<a href="{{ url('news/detail/' . $row->slug) }}">{{$row->title}}</a>
											</h5>
											<!--/ Title -->
										</div>
										<!--/ Related article post .rta-post -->
									</div>
									@endforeach
									<!--/ col-sm-4 -->
								</div>
								<!--/ row -->
							</div>
							<!--/ Related articles -->
						</div>
						<!--/ Post layout -->
					</div>
					<!--/ Post div wrapper -->
					</div>
					<!--/ Post comments wrapper -->
				</div>
				<!--/ Post content -->
			</div>	
			<!--/ col-md-9 col-sm-9 -->
		</div>
		<!--/ row -->
	</div>
	<!--/ container -->
</section>

@endsection