<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\{Product};
use Illuminate\Support\Facades\Input;
use DB;

class ProductController extends Controller
{
    public function index(){
        $product = Input::get('catalog');
        $type = DB::table('m_types')->where('slug', $product)->value('id');
        $data = Product::where('type', $type)->get();
        
        return view('frontend.product.list-product', compact('data', 'product'));
    }

    public function detail($id){
    	$product = Product::leftjoin('m_types AS b', 'products.type', 'b.id')->where('products.slug', $id)->first();
    	return view('frontend.product.detail', compact('product'));
    }
}
