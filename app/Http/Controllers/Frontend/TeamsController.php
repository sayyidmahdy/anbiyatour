<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\{Teams};

class TeamsController extends Controller
{
    public function index(){
    	$teams = Teams::get();
    	return view('frontend.teams.index', compact('teams'));
    }
}
