<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\{News};

class NewsController extends Controller
{
    public function index(){
    	$news = News::get();
    	return view('frontend.news.index', compact('news'));
    }

    public function detail($id){
    	$news = News::where('slug', $id)->first();

    	$date = date_create($news->created_at);
		$created_at = date_format($date,"l d-M-Y");
		$latest_news = News::limit('3')->get();
    	return view('frontend.news.detail', compact('news', 'created_at', 'latest_news'));
    }
}
