<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\{Product, M_type, M_flight, Slideshow, Testimonials, Teams, News};

class HomeController extends Controller
{
    public function index(){

    	$data['type'] = M_type::get();
    	$data['product'] = Product::where('status', 1)->get();
    	$data['slideshow'] = Slideshow::where('status', 1)->get();
    	$data['testimonials'] = Testimonials::limit(3)->get();
    	$data['latest_product'] = Product::where('status', 1)->orderby('created_at', 'desc')->limit(9)->get();
        $data['teams'] = Teams::get();
        $data['news'] = News::get();
        $data['flight'] = M_flight::get();

        // dd($data['product']);
        return view('frontend.dashboard', $data);
    }

    public function menu_product(){
    	$data = M_type::get();
    	return \Response::json($data);
    }
}
