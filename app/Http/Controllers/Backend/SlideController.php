<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use App\Model\{Slideshow};
use Session, File, Validator;

class SlideController extends Controller
{
    public function index()
    {
        $data = Slideshow::leftjoin('m_status AS b', 'slideshows.status', 'b.id_status')->select('slideshows.*', 'b.status_name')->get();
        return view('backend.slideshow.index', compact('data'));
    }

    public function create()
    {
        $act = 'add';
        return view('backend.slideshow.create', compact('act'));
    }

    public function store(Request $request)
    {
        $rules = [
            'image' => 'mimes:jpeg,jpg,png|required|max:3000'
        ];

        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails())
        {
            Session::flash('message', 'error|upload image slideshow gagal! file type only jpeg, jpg, png and max size 3mb');
            return redirect('backend/slideshow');
        }

        $file   = Input::file('image');
        $fileSize      = $file->getClientSize();
        $fileExtension = $file->getClientOriginalExtension();
        $fileName      = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);

        $path = public_path() . '/backend/image/slideshow/';
        $date = date("Ymd");
        $time = date("His");
        $newfileName = $fileName . '-' . $date . '-' . $time . '.' . $fileExtension;

        $slideshow = new slideshow;
        $slideshow->title       = Input::get('title');
        $slideshow->image       = $newfileName;
        $slideshow->image_path  = $path;
        $slideshow->status = 1;
        $slideshow->save();

        if ($slideshow) {
            $moveFile = $file->move($path, $newfileName);
            Session::flash('message', 'success|Simpan data slideshow berhasil!');
        } else {
            Session::flash('message', 'error|Simpan data slideshow gagal!');
        }

        return redirect('backend/slideshow');
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $act = 'edit';
        $data = Slideshow::where('slug', $id)->first();
        
        return view('backend.slideshow.create', compact('act', 'data'));
    }

    public function update(Request $request, $id)
    {
        $file   = Input::file('image');
        if ($file) {
            $rules = [
                'image' => 'mimes:jpeg,jpg,png|required|max:3000'
            ];

            $validator = Validator::make(Input::all(), $rules);
            if ($validator->fails())
            {
                Session::flash('message', 'error|upload image slideshow gagal! file type only jpeg, jpg, png and max size 3mb');
                return redirect('backend/slideshow');
            }

            $fileSize      = $file->getClientSize();
            $fileExtension = $file->getClientOriginalExtension();
            $fileName      = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);

            $path = public_path() . '/backend/image/slideshow/';
            $date = date("Ymd");
            $time = date("His");
            $newfileName = $fileName . '-' . $date . '-' . $time . '.' . $fileExtension;

            $data = Slideshow::find($id)->value('image');
            $old_image = $path . $data;
        }
        
        $slideshow = Slideshow::find($id);
        $slideshow->title   = Input::get('title');

        if($file){
            $slideshow->image       = $newfileName;
            $slideshow->image_path  = $path;
            $slideshow->save();
            $moveFile = $file->move($path, $newfileName);
            $deleteImage = File::delete($old_image);
        } else {
            $slideshow->save();
        }

        if ($slideshow) {
            Session::flash('message', 'success|Simpan data slideshow berhasil!');
        } else {
            Session::flash('message', 'error|Simpan data slideshow gagal!');
        }

        return redirect('backend/slideshow');
    }

    public function destroy($id)
    {
        $data      = Slideshow::find($id)->value('image');
        $path      = public_path() . '/backend/image/slideshow/';
        $old_image = $path . $data;
        
        $delete    = Slideshow::where('id', $id)->delete();
        if ($delete) {
            File::delete($old_image);
            Session::flash('message', 'success|Hapus data slideshow berhasil!');
            return 'true';
        } else {
            Session::flash('message', 'error|Hapus data slideshow gagal!');
            return 'false';
        }
    }
}
