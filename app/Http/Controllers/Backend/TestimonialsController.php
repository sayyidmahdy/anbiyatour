<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use App\Model\{Testimonials};
use Session, File, Validator;

class TestimonialsController extends Controller
{
    public function index()
    {
        $data = Testimonials::leftjoin('m_status AS b', 'testimonials.status', 'b.id_status')->select('testimonials.*', 'b.status_name')->get();
        return view('backend.testimonials.index', compact('data'));
    }

    public function create()
    {
        $act = 'add';
        return view('backend.testimonials.create', compact('act'));
    }

    public function store(Request $request)
    {
        $rules = [
            'image' => 'mimes:jpeg,jpg,png|required|max:3000'
        ];

        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails())
        {
            Session::flash('message', 'error|upload image testimonials gagal! file type only jpeg, jpg, png and max size 3mb');
            return redirect('backend/testimonials');
        }

        $file   = Input::file('image');
        $fileSize      = $file->getClientSize();
        $fileExtension = $file->getClientOriginalExtension();
        $fileName      = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);

        $path = public_path() . '/backend/image/testimonials/';
        $date = date("Ymd");
        $time = date("His");
        $newfileName = $fileName . '-' . $date . '-' . $time . '.' . $fileExtension;

        $testimonials = new testimonials;
        $testimonials->user        = Input::get('user');
        $testimonials->company     = Input::get('company');
        $testimonials->image       = $newfileName;
        $testimonials->image_path  = $path;
        $testimonials->status = 1;
        $testimonials->save();

        if ($testimonials) {
            $moveFile = $file->move($path, $newfileName);
            Session::flash('message', 'success|Simpan data testimonials berhasil!');
        } else {
            Session::flash('message', 'error|Simpan data testimonials gagal!');
        }

        return redirect('backend/testimonials');
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $act = 'edit';
        $data = Testimonials::where('slug', $id)->first();
        
        return view('backend.testimonials.create', compact('act', 'data'));
    }

    public function update(Request $request, $id)
    {
        $file   = Input::file('image');
        if ($file) {
            $rules = [
                'image' => 'mimes:jpeg,jpg,png|required|max:3000'
            ];

            $validator = Validator::make(Input::all(), $rules);
            if ($validator->fails())
            {
                Session::flash('message', 'error|upload image testimonials gagal! file type only jpeg, jpg, png and max size 3mb');
                return redirect('backend/testimonials');
            }

            $fileSize      = $file->getClientSize();
            $fileExtension = $file->getClientOriginalExtension();
            $fileName      = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);

            $path = public_path() . '/backend/image/testimonials/';
            $date = date("Ymd");
            $time = date("His");
            $newfileName = $fileName . '-' . $date . '-' . $time . '.' . $fileExtension;

            $data = Testimonials::find($id)->value('image');
            $old_image = $path . $data;
        }
        
        $testimonials = Testimonials::find($id);
        $testimonials->user     = Input::get('user');
        $testimonials->company  = Input::get('company');

        if($file){
            $testimonials->image       = $newfileName;
            $testimonials->image_path  = $path;
            $testimonials->save();
            $moveFile = $file->move($path, $newfileName);
            $deleteImage = File::delete($old_image);
        } else {
            $testimonials->save();
        }

        if ($testimonials) {
            Session::flash('message', 'success|Simpan data testimonials berhasil!');
        } else {
            Session::flash('message', 'error|Simpan data testimonials gagal!');
        }

        return redirect('backend/testimonials');
    }

    public function destroy($id)
    {
        $data      = Testimonials::find($id)->value('image');
        $path      = public_path() . '/backend/image/testimonials/';
        $old_image = $path . $data;
        
        $delete    = Testimonials::where('id', $id)->delete();
        if ($delete) {
            File::delete($old_image);
            Session::flash('message', 'success|Hapus data testimonials berhasil!');
            return 'true';
        } else {
            Session::flash('message', 'error|Hapus data testimonials gagal!');
            return 'false';
        }
    }}
