<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use App\Model\{Teams};
use Session, File, Validator;

class TeamsController extends Controller
{
    public function index()
    {
        $data = Teams::leftjoin('m_status AS b', 'teams.status', 'b.id_status')->select('teams.*', 'b.status_name')->get();
        return view('backend.teams.index', compact('data'));
    }

    public function create()
    {
        $act = 'add';
        return view('backend.teams.create', compact('act'));
    }

    public function store(Request $request)
    {
        $rules = [
            'image' => 'mimes:jpeg,jpg,png|required|max:3000'
        ];

        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails())
        {
            Session::flash('message', 'error|upload image teams gagal! file type only jpeg, jpg, png and max size 3mb');
            return redirect('backend/teams');
        }

        $file   = Input::file('image');
        $fileSize      = $file->getClientSize();
        $fileExtension = $file->getClientOriginalExtension();
        $fileName      = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);

        $path = public_path() . '/backend/image/teams/';
        $date = date("Ymd");
        $time = date("His");
        $newfileName = $fileName . '-' . $date . '-' . $time . '.' . $fileExtension;

        $teams = new teams;
        $teams->name        = Input::get('name');
        $teams->position    = Input::get('position');
        $teams->image       = $newfileName;
        $teams->image_path  = $path;
        $teams->status = 1;
        $teams->save();

        if ($teams) {
            $moveFile = $file->move($path, $newfileName);
            Session::flash('message', 'success|Simpan data teams berhasil!');
        } else {
            Session::flash('message', 'error|Simpan data teams gagal!');
        }

        return redirect('backend/teams');
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $act = 'edit';
        $data = Teams::where('slug', $id)->first();
        
        return view('backend.teams.create', compact('act', 'data'));
    }

    public function update(Request $request, $id)
    {
        $file   = Input::file('image');
        if ($file) {
            $rules = [
                'image' => 'mimes:jpeg,jpg,png|required|max:3000'
            ];

            $validator = Validator::make(Input::all(), $rules);
            if ($validator->fails())
            {
                Session::flash('message', 'error|upload image teams gagal! file type only jpeg, jpg, png and max size 3mb');
                return redirect('backend/teams');
            }

            $fileSize      = $file->getClientSize();
            $fileExtension = $file->getClientOriginalExtension();
            $fileName      = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);

            $path = public_path() . '/backend/image/teams/';
            $date = date("Ymd");
            $time = date("His");
            $newfileName = $fileName . '-' . $date . '-' . $time . '.' . $fileExtension;

            $data = Teams::find($id)->value('image');
            $old_image = $path . $data;
        }
        
        $teams = Teams::find($id);
        $teams->name     = Input::get('name');
        $teams->position = Input::get('position');

        if($file){
            $teams->image       = $newfileName;
            $teams->image_path  = $path;
            $teams->save();
            $moveFile = $file->move($path, $newfileName);
            $deleteImage = File::delete($old_image);
        } else {
            $teams->save();
        }

        if ($teams) {
            Session::flash('message', 'success|Simpan data teams berhasil!');
        } else {
            Session::flash('message', 'error|Simpan data teams gagal!');
        }

        return redirect('backend/teams');
    }

    public function destroy($id)
    {
        $data      = Teams::find($id)->value('image');
        $path      = public_path() . '/backend/image/teams/';
        $old_image = $path . $data;
        
        $delete    = Teams::where('id', $id)->delete();
        if ($delete) {
            File::delete($old_image);
            Session::flash('message', 'success|Hapus data teams berhasil!');
            return 'true';
        } else {
            Session::flash('message', 'error|Hapus data teams gagal!');
            return 'false';
        }
    }
}
