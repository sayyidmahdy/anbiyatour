<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use App\Model\News;
use Session, File, Validator;

class NewsController extends Controller
{
    public function index()
    {
        $data = News::leftjoin('m_status AS b', 'news.status', 'b.id_status')->select('news.*', 'b.status_name')->get();
        return view('backend.news.index', compact('data'));
    }

    public function create()
    {
        $act = 'add';
        return view('backend.news.create', compact('act'));
    }

    public function store(Request $request)
    {
        $rules = [
            'image' => 'mimes:jpeg,jpg,png|required|max:3000'
        ];

        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails())
        {
            Session::flash('message', 'error|upload image news gagal! file type only jpeg, jpg, png and max size 3mb');
            return redirect('backend/news');
        }

        $file   = Input::file('image');
        $fileSize      = $file->getClientSize();
        $fileExtension = $file->getClientOriginalExtension();
        $fileName      = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);

        $path = public_path() . '/backend/image/news/';
        $date = date("Ymd");
        $time = date("His");
        $newfileName = $fileName . '-' . $date . '-' . $time . '.' . $fileExtension;

        $news = new News;
        $news->title       = Input::get('title');
        $news->content     = Input::get('content');
        $news->image       = $newfileName;
        $news->image_path  = $path;
        $news->status = 1;
        $news->save();

        if ($news) {
            $moveFile = $file->move($path, $newfileName);
            Session::flash('message', 'success|Simpan data news berhasil!');
        } else {
            Session::flash('message', 'error|Simpan data news gagal!');
        }

        return redirect('backend/news');
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $act = 'edit';
        $data = News::where('slug', $id)->first();
        
        return view('backend.news.create', compact('act', 'data'));
    }

    public function update(Request $request, $id)
    {
        $file   = Input::file('image');
        if ($file) {
            $rules = [
                'image' => 'mimes:jpeg,jpg,png|required|max:3000'
            ];

            $validator = Validator::make(Input::all(), $rules);
            if ($validator->fails())
            {
                Session::flash('message', 'error|upload image news gagal! file type only jpeg, jpg, png and max size 3mb');
                return redirect('backend/news');
            }

            $fileSize      = $file->getClientSize();
            $fileExtension = $file->getClientOriginalExtension();
            $fileName      = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);

            $path = public_path() . '/backend/image/news/';
            $date = date("Ymd");
            $time = date("His");
            $newfileName = $fileName . '-' . $date . '-' . $time . '.' . $fileExtension;

            $data = News::find($id)->value('image');
            $old_image = $path . $data;
        }
        
        $news = News::find($id);
        $news->title   = Input::get('title');
        $news->content = Input::get('content');

        if($file){
            $news->image       = $newfileName;
            $news->image_path  = $path;
            $news->save();
            $moveFile = $file->move($path, $newfileName);
            $deleteImage = File::delete($old_image);
        } else {
            $news->save();
        }

        if ($news) {
            Session::flash('message', 'success|Simpan data news berhasil!');
        } else {
            Session::flash('message', 'error|Simpan data news gagal!');
        }

        return redirect('backend/news');
    }

    public function destroy($id)
    {
        $data      = News::find($id)->value('image');
        $path      = public_path() . '/backend/image/news/';
        $old_image = $path . $data;
        
        $delete    = News::where('id', $id)->delete();
        if ($delete) {
            File::delete($old_image);
            Session::flash('message', 'success|Hapus data news berhasil!');
            return 'true';
        } else {
            Session::flash('message', 'error|Hapus data news gagal!');
            return 'false';
        }
    }
}
