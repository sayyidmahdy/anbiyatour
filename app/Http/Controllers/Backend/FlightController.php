<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use App\Model\{M_flight};
use Session, File, Validator;

class FlightController extends Controller
{
    public function index()
    {
        $data = M_flight::get();
        return view('backend.flight.index', compact('data'));
    }

    public function create()
    {
        $act = 'add';
        return view('backend.flight.create', compact('act'));
    }

    public function store(Request $request)
    {
        $rules = [
            'image' => 'mimes:jpeg,jpg,png|required|max:3000'
        ];

        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails())
        {
            Session::flash('message', 'error|upload image flight gagal! file type only jpeg, jpg, png and max size 3mb');
            return redirect('backend/flight');
        }

        $file   = Input::file('image');
        $fileSize      = $file->getClientSize();
        $fileExtension = $file->getClientOriginalExtension();
        $fileName      = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);

        $path = public_path() . '/backend/image/flight/';
        $date = date("Ymd");
        $time = date("His");
        $newfileName = $fileName . '-' . $date . '-' . $time . '.' . $fileExtension;

        $flight = new M_flight;
        $flight->name        = Input::get('name');
        $flight->image       = $newfileName;
        $flight->image_path  = $path;
        $flight->status = 1;
        $flight->save();

        if ($flight) {
            $moveFile = $file->move($path, $newfileName);
            Session::flash('message', 'success|Simpan data flight berhasil!');
        } else {
            Session::flash('message', 'error|Simpan data flight gagal!');
        }

        return redirect('backend/flight');
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $act = 'edit';
        $data = M_flight::where('slug', $id)->first();
        
        return view('backend.flight.create', compact('act', 'data'));
    }

    public function update(Request $request, $id)
    {
        $file   = Input::file('image');
        if ($file) {
            $rules = [
                'image' => 'mimes:jpeg,jpg,png|required|max:3000'
            ];

            $validator = Validator::make(Input::all(), $rules);
            if ($validator->fails())
            {
                Session::flash('message', 'error|upload image flight gagal! file type only jpeg, jpg, png and max size 3mb');
                return redirect('backend/flight');
            }

            $fileSize      = $file->getClientSize();
            $fileExtension = $file->getClientOriginalExtension();
            $fileName      = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);

            $path = public_path() . '/backend/image/flight/';
            $date = date("Ymd");
            $time = date("His");
            $newfileName = $fileName . '-' . $date . '-' . $time . '.' . $fileExtension;

            $data = M_flight::find($id)->value('image');
            $old_image = $path . $data;
        }
        
        $flight = M_flight::find($id);
        $flight->name     = Input::get('name');

        if($file){
            $flight->image       = $newfileName;
            $flight->image_path  = $path;
            $flight->save();
            $moveFile = $file->move($path, $newfileName);
            $deleteImage = File::delete($old_image);
        } else {
            $flight->save();
        }

        if ($flight) {
            Session::flash('message', 'success|Simpan data flight berhasil!');
        } else {
            Session::flash('message', 'error|Simpan data flight gagal!');
        }

        return redirect('backend/flight');
    }

    public function destroy($id)
    {
        $data      = M_flight::find($id)->value('image');
        $path      = public_path() . '/backend/image/flight/';
        $old_image = $path . $data;
        
        $delete    = M_flight::where('id', $id)->delete();
        if ($delete) {
            File::delete($old_image);
            Session::flash('message', 'success|Hapus data flight berhasil!');
            return 'true';
        } else {
            Session::flash('message', 'error|Hapus data flight gagal!');
            return 'false';
        }
    }
}
