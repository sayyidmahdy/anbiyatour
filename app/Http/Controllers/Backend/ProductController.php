<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use App\Model\{Product, M_type, M_flight};
use Session, File, Validator;

class ProductController extends Controller
{
    public function index()
    {
        $data = Product::leftjoin('m_status AS b', 'products.status', 'b.id_status')->select('products.*', 'b.status_name')->get();
        return view('backend.product.index', compact('data'));
    }

    public function create()
    {
        $act = 'add';
        $type = M_type::get();
        $flight = M_flight::get();
        return view('backend.product.create', compact('act', 'type', 'flight'));
    }

    public function store(Request $request)
    {
        $rules = [
            'image' => 'mimes:jpeg,jpg,png,svg|required|max:3000'
        ];

        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails())
        {
            Session::flash('message', 'error|upload image product gagal! file type only jpeg, jpg, png and max size 3mb');
            return redirect('backend/product');
        }

        $file   = Input::file('image');
        $fileSize      = $file->getClientSize();
        $fileExtension = $file->getClientOriginalExtension();
        $fileName      = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);

        $path = public_path() . '/backend/image/product/';
        $date = date("Ymd");
        $time = date("His");
        $newfileName = $fileName . '-' . $date . '-' . $time . '.' . $fileExtension;

        $product = new Product;
        $product->title       = Input::get('title');
        $product->description = Input::get('description');
        $product->price       = Input::get('price');
        $product->flight      = Input::get('flight');
        $product->type        = Input::get('type');
        $product->qty         = Input::get('qty');
        $product->image       = $newfileName;
        $product->image_path  = $path;
        $product->save();

        if ($product) {
            $moveFile = $file->move($path, $newfileName);
            Session::flash('message', 'success|Simpan data product berhasil!');
        } else {
            Session::flash('message', 'error|Simpan data product gagal!');
        }

        return redirect('backend/product');
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $act = 'edit';
        $data = Product::where('slug', $id)->first();
        $type = M_type::get();
        $flight = M_flight::get();
        
        return view('backend.product.create', compact('act', 'data', 'type', 'flight'));
    }

    public function update(Request $request, $id)
    {
        $file   = Input::file('image');
        if ($file) {
            $rules = [
                'image' => 'mimes:jpeg,jpg,png|required|max:3000'
            ];

            $validator = Validator::make(Input::all(), $rules);
            if ($validator->fails())
            {
                Session::flash('message', 'error|upload image product gagal! file type only jpeg, jpg, png and max size 3mb');
                return redirect('backend/product');
            }

            $fileSize      = $file->getClientSize();
            $fileExtension = $file->getClientOriginalExtension();
            $fileName      = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);

            $path = public_path() . '/backend/image/product/';
            $date = date("Ymd");
            $time = date("His");
            $newfileName = $fileName . '-' . $date . '-' . $time . '.' . $fileExtension;

            $data = Product::find($id)->value('image');
            $old_image = $path . $data;
        }
        
        $product = Product::find($id);
        $product->title       = Input::get('title');
        $product->description = Input::get('description');
        $product->price       = Input::get('price');
        $product->flight      = Input::get('flight');
        $product->type        = Input::get('type');
        $product->qty         = Input::get('qty');

        if($file){
            $product->image       = $newfileName;
            $product->image_path  = $path;
            $product->save();
            $moveFile = $file->move($path, $newfileName);
            $deleteImage = File::delete($old_image);
        } else {
            $product->save();
        }

        if ($product) {
            Session::flash('message', 'success|Simpan data product berhasil!');
        } else {
            Session::flash('message', 'error|Simpan data product gagal!');
        }

        return redirect('backend/product');
    }

    public function destroy($id)
    {
        $data      = Product::find($id)->value('image');
        $path      = public_path() . '/backend/image/product/';
        $old_image = $path . $data;
        
        $delete    = Product::where('id', $id)->delete();
        if ($delete) {
            File::delete($old_image);
            Session::flash('message', 'success|Hapus data product berhasil!');
            return 'true';
        } else {
            Session::flash('message', 'error|Hapus data product gagal!');
            return 'false';
        }
    }
}
