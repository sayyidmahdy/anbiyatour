<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use App\Model\{M_type};
use Session, File, Validator;

class TypeController extends Controller
{
    public function index()
    {
        $data = M_type::get();
        return view('backend.type.index', compact('data'));
    }

    public function create()
    {
        $act = 'add';
        return view('backend.type.create', compact('act'));
    }

    public function store(Request $request)
    {
        $type = new M_type;
        $type->name = Input::get('name');
        $type->save();

        if ($type) {
            Session::flash('message', 'success|Simpan data category berhasil!');
        } else {
            Session::flash('message', 'error|Simpan data category gagal!');
        }

        return redirect('backend/category');
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $act = 'edit';
        $data = M_type::where('slug', $id)->first();
        
        return view('backend.type.create', compact('act', 'data'));
    }

    public function update(Request $request, $id)
    {
        
        $type = M_type::find($id);
        $type->name = Input::get('name');
        $type->save();

        if ($type) {
            Session::flash('message', 'success|Simpan data category berhasil!');
        } else {
            Session::flash('message', 'error|Simpan data category gagal!');
        }

        return redirect('backend/category');
    }

    public function destroy($id)
    {
        
        $delete    = M_type::where('id', $id)->delete();
        if ($delete) {
            Session::flash('message', 'success|Hapus data category berhasil!');
            return 'true';
        } else {
            Session::flash('message', 'error|Hapus data category gagal!');
            return 'false';
        }
    }
}
